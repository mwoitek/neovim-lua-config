local ft_kb = vim.api.nvim_create_augroup("FiletypeKeybindings", {})

vim.api.nvim_create_autocmd("FileType", {
  group = ft_kb,
  pattern = "go",
  callback = function()
    vim.keymap.set("i", "]a", "->", { buffer = 0 })
    vim.keymap.set("i", "]b", "<-", { buffer = 0 })
    vim.keymap.set("i", "]c", ":=", { buffer = 0 })
  end,
})

vim.api.nvim_create_autocmd("FileType", {
  group = ft_kb,
  pattern = "rust",
  callback = function() vim.keymap.set("i", "]a", "->", { buffer = 0 }) end,
})

vim.api.nvim_create_autocmd("FileType", {
  group = ft_kb,
  pattern = { "r", "rmd" },
  callback = function() vim.keymap.set("i", "]p", "%>%", { buffer = 0 }) end,
})

local function add_semicolon()
  local row = vim.api.nvim_win_get_cursor(0)[1]
  local line = vim.api.nvim_buf_get_lines(0, row - 1, row, false)[1]
  if vim.endswith(line, ";") then return end
  vim.api.nvim_buf_set_lines(0, row - 1, row, false, { line .. ";" })
end

vim.api.nvim_create_autocmd("FileType", {
  group = ft_kb,
  pattern = "rust",
  callback = function()
    vim.keymap.set("i", "]b", "<><left>", { buffer = 0 })
    vim.keymap.set("i", "]c", "::", { buffer = 0 })
    vim.keymap.set("i", "]t", "::<><left>", { buffer = 0 })
    vim.keymap.set("n", "<leader>s", add_semicolon, { buffer = 0 })
  end,
})
