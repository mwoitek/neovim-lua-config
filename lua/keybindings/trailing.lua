vim.keymap.set("n", "<space>cw", function()
  local trailing = require "utils.trailing"
  trailing.delete_trailing_whitespace()
  trailing.delete_trailing_lines()
end, {})
