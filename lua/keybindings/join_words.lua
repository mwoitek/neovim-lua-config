local function to_camel_case(text)
  return vim.tbl_map(
    function(l) return (vim.trim(l):lower():gsub("%s+%l", string.upper):gsub("%s+", "")) end,
    text
  )
end

local function to_pascal_case(text)
  return vim.tbl_map(
    function(l) return (vim.trim(l):lower():gsub("%f[%l].", string.upper):gsub("%s+", "")) end,
    text
  )
end

local function to_snake_case(text)
  return vim.tbl_map(function(l) return (vim.trim(l):lower():gsub("%s+", "_")) end, text)
end

local function with_dots(text)
  return vim.tbl_map(function(l) return (vim.trim(l):gsub("%s+", [[.]])) end, text)
end

vim.keymap.set(
  "x",
  "<space>jc",
  function() require("utils.visual_selection").transform_text(to_camel_case) end,
  {}
)

vim.keymap.set(
  "x",
  "<space>jp",
  function() require("utils.visual_selection").transform_text(to_pascal_case) end,
  {}
)

vim.keymap.set(
  "x",
  "<space>js",
  function() require("utils.visual_selection").transform_text(to_snake_case) end,
  {}
)

vim.keymap.set(
  "x",
  "<space>jd",
  function() require("utils.visual_selection").transform_text(with_dots) end,
  {}
)
