-- vim: set foldmethod=marker:

-- Insert delimiters {{{
---@param commentstring string
---@param label string
local _insert_fold_delimiters = function(commentstring, label)
  label = vim.trim(label) .. " "
  local fold_start, fold_end = unpack(vim.split(vim.wo.foldmarker, ","))
  local new_lines = {
    commentstring:format(vim.trim(label .. fold_start)),
    "",
    commentstring:format(fold_end),
  }
  local curr_line = vim.api.nvim_win_get_cursor(0)[1]
  vim.api.nvim_buf_set_lines(0, curr_line - 1, curr_line, true, new_lines)
  vim.api.nvim_win_set_cursor(0, { curr_line + 1, 0 })
  vim.cmd "normal! zv"
end

local insert_fold_delimiters = function()
  if vim.wo.foldmethod ~= "marker" then
    vim.notify("This is meant to be used when foldmethod=marker", vim.log.levels.WARN)
    return
  end
  local commentstring = vim.bo.commentstring
  if commentstring == "" then
    vim.notify("No comment string for the current buffer", vim.log.levels.WARN)
    return
  end
  vim.ui.input({ prompt = "Enter fold label: " }, function(input)
    if not input then return end
    _insert_fold_delimiters(commentstring, input)
  end)
end

vim.keymap.set("n", "<space>fm", insert_fold_delimiters, {})
-- }}}

-- Wrap with delimiters {{{
---@param commentstring string
---@param range integer[]
---@param label string
local _wrap_with_delimiters = function(commentstring, range, label)
  local first_line, last_line = unpack(range)
  label = vim.trim(label) .. " "
  local fold_start, fold_end = unpack(vim.split(vim.wo.foldmarker, ","))
  local new_lines = {
    { commentstring:format(vim.trim(label .. fold_start)) },
    { commentstring:format(fold_end) },
  }
  vim.api.nvim_buf_set_lines(0, last_line + 1, last_line + 1, false, new_lines[2])
  vim.api.nvim_buf_set_lines(0, first_line, first_line, false, new_lines[1])
  vim.cmd "normal! zx[zzv"
end

local wrap_with_delimiters = function()
  if vim.api.nvim_get_mode().mode ~= "V" then
    vim.notify("This is meant to be used in linewise visual mode", vim.log.levels.WARN)
    return
  end
  if vim.wo.foldmethod ~= "marker" then
    vim.notify("This is meant to be used when foldmethod=marker", vim.log.levels.WARN)
    return
  end
  local commentstring = vim.bo.commentstring
  if commentstring == "" then
    vim.notify("No comment string for the current buffer", vim.log.levels.WARN)
    return
  end
  local first_line, _, last_line, _ = unpack(require("utils.visual_selection").get_range())
  local range = { first_line, last_line }
  vim.ui.input({ prompt = "Enter fold label: " }, function(input)
    if not input then return end
    _wrap_with_delimiters(commentstring, range, input)
  end)
end

vim.keymap.set("v", "<space>fm", wrap_with_delimiters, {})
-- }}}

-- Fold textobject {{{
---@param inner boolean?
local _fold_textobject = function(inner)
  local mode = vim.api.nvim_get_mode().mode
  if mode:lower() ~= "v" then return end
  vim.cmd("normal! " .. mode)
  local curr_line = vim.api.nvim_win_get_cursor(0)[1]
  if vim.fn.foldlevel(curr_line) == 0 then
    vim.cmd "normal! zj"
    curr_line = vim.api.nvim_win_get_cursor(0)[1]
  end
  if vim.fn.foldlevel(curr_line) == 0 then return end
  vim.cmd "normal! zc"
  local fold_start = vim.fn.foldclosed(curr_line)
  if fold_start == -1 then
    vim.cmd "normal! zo"
    return
  end
  local fold_end = vim.fn.foldclosedend(curr_line)
  if vim.wo.foldmethod == "marker" and inner then
    fold_start = fold_start + 1
    fold_end = fold_end - 1
  end
  vim.api.nvim_win_set_cursor(0, { fold_start, 0 })
  vim.cmd "normal! zoV"
  local last_col = vim.fn.col { fold_end, "$" } - 1
  vim.api.nvim_win_set_cursor(0, { fold_end, last_col })
end

---@param inner boolean?
local fold_textobject = function(inner)
  local old = vim.o.lazyredraw
  vim.o.lazyredraw = true
  _fold_textobject(inner)
  vim.o.lazyredraw = old
end

vim.keymap.set("x", "ad", fold_textobject, {})
vim.keymap.set("x", "id", function() fold_textobject(true) end, {})
vim.keymap.set("o", "ad", "<cmd>normal vad<cr>", {})
vim.keymap.set("o", "id", "<cmd>normal vid<cr>", {})
-- }}}
