local function set_buf_vars(lang)
  vim.b[0].spell_lang = vim.wo[0].spell and (string.gsub(lang:upper(), "_", "-")) or nil

  if not vim.b[0].spell_lang then
    vim.b[0].spell_fg = nil
    vim.b[0].spell_bg = nil
    return
  end

  if lang == "en_us" then
    vim.b[0].spell_fg = "#ff0000"
    vim.b[0].spell_bg = "#3c3b6e"
  elseif lang == "pt_br" then
    vim.b[0].spell_fg = "#fedf00"
    vim.b[0].spell_bg = "#009b3a"
  end
end

local function toggle_spellchecker(lang)
  if type(lang) ~= "string" then return end
  if lang ~= "en_us" and lang ~= "pt_br" then return end

  if vim.wo[0].spell then
    if lang == vim.bo[0].spelllang then
      vim.wo[0].spell = false
    else
      vim.bo[0].spelllang = lang
    end
  else
    vim.wo[0].spell = true
    vim.bo[0].spelllang = lang
  end

  set_buf_vars(lang)
end

vim.keymap.set("n", "<leader>i", function() toggle_spellchecker "en_us" end, {})
vim.keymap.set("n", "<leader>p", function() toggle_spellchecker "pt_br" end, {})
