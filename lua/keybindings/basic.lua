vim.keymap.set("n", ",", "", {})
vim.g.mapleader = ","

for _, char in ipairs { "!", ",", ".", ":", ";", "?" } do
  vim.keymap.set("i", char, char .. "<c-g>u", { noremap = true })
end

vim.keymap.set("i", "]d", "$", {})
vim.keymap.set("i", "]e", "&", {})
vim.keymap.set("i", "]r", "%", {})

vim.keymap.set("n", "<leader>Q", "<cmd>quitall<cr>", {})
vim.keymap.set("n", "<leader>q", "<cmd>q<cr>", {})
vim.keymap.set("n", "<leader>z", "<cmd>wq<cr>", {})
vim.keymap.set("n", "<space>fs", "<cmd>w<cr>", {})

vim.keymap.set("n", "<space>fo", "zMzv", {})

vim.keymap.set("n", "<space>d", "<c-d>zz", {})
vim.keymap.set("n", "<space>u", "<c-u>zz", {})

vim.keymap.set("n", "<space>bk", "<cmd>bd!<cr>", {})

vim.keymap.set("n", "<space>wd", "<c-w>c", {})
vim.keymap.set("n", "<space>wh", "<c-w>h", {})
vim.keymap.set("n", "<space>wj", "<c-w>j", {})
vim.keymap.set("n", "<space>wk", "<c-w>k", {})
vim.keymap.set("n", "<space>wl", "<c-w>l", {})

vim.keymap.set("n", "<space>ws", "<cmd>sp<cr>", {})
vim.keymap.set("n", "<space>wv", "<cmd>vs<cr>", {})

vim.keymap.set("n", "N", "Nzz", { noremap = true })
vim.keymap.set("n", "n", "nzz", { noremap = true })

vim.keymap.set("n", "Y", "y$", {})
vim.keymap.set("v", "p", "pgvy", { noremap = true })

vim.keymap.set("n", "g-", "<c-x>", {})
vim.keymap.set("v", "g-", "g<c-x>", {})

vim.keymap.set("n", "g=", "<c-a>", {})
vim.keymap.set("v", "g=", "g<c-a>", {})

vim.keymap.set({ "n", "o" }, "gl", "$", {})
vim.keymap.set("v", "gl", "$<left>", {})

vim.keymap.set("n", "gb", "i<cr><esc>^", {})
vim.keymap.set("n", "gm", "", {})

vim.keymap.set({ "n", "o", "v" }, "<space>]", "%", {})

vim.keymap.set("v", "<", "<gv", { noremap = true })
vim.keymap.set("v", ">", ">gv", { noremap = true })

vim.keymap.set("x", "al", "$o0", {})
vim.keymap.set("x", "il", "g_o^", {})
vim.keymap.set("o", "al", "<cmd>normal val<cr>", {})
vim.keymap.set("o", "il", "<cmd>normal vil<cr>", {})
