vim.keymap.set("n", "<space>gg", "<cmd>Gedit :<cr>", { silent = true })

-- Keybindings for lazy.nvim
vim.keymap.set("n", "<space>pc", "<cmd>Lazy clean<cr>", { silent = true })
vim.keymap.set("n", "<space>pp", "<cmd>Lazy profile<cr>", { silent = true })
vim.keymap.set("n", "<space>py", "<cmd>Lazy sync<cr>", { silent = true })
