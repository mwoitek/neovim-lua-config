local keybindings_files = {
  "basic",
  "folds",
  "spell",
  "join_words",
  "trailing",
  "language_specific",
  "plugins",
}
local add_prefix_and_load = require("utils.load_files").add_prefix_and_load
add_prefix_and_load(keybindings_files, "keybindings.")
