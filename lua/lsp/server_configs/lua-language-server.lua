local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("lua-language-server", function()
  local common_setup = require "lsp.common_setup"
  require("lspconfig").lua_ls.setup {
    capabilities = common_setup.capabilities,
    on_attach = function(client, bufnr)
      client.server_capabilities.documentFormattingProvider = false
      client.server_capabilities.documentRangeFormattingProvider = false
      common_setup.on_attach(client, bufnr)
    end,
    settings = {
      Lua = {
        diagnostics = { workspaceDelay = -1 },
        telemetry = { enable = false },
      },
    },
  }
end)
