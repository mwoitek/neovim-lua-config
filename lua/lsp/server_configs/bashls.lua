local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("bash-language-server", function()
  local common_setup = require "lsp.common_setup"
  require("lspconfig").bashls.setup {
    capabilities = common_setup.capabilities,
    on_attach = function(client, bufnr)
      client.server_capabilities.documentFormattingProvider = false
      common_setup.on_attach(client, bufnr)
    end,
  }
end)
