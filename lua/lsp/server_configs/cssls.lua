local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("vscode-css-language-server", function()
  local common_setup = require "lsp.common_setup"

  local capabilities = common_setup.capabilities
  capabilities.textDocument.completion.completionItem.snippetSupport = true

  require("lspconfig").cssls.setup {
    capabilities = capabilities,
    on_attach = common_setup.on_attach,
    init_options = { provideFormatter = false },
  }
end)
