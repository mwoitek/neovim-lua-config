local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("rust-analyzer", function()
  local common_setup = require "lsp.common_setup"
  require("lspconfig").rust_analyzer.setup {
    capabilities = common_setup.capabilities,
    on_attach = common_setup.on_attach,
    settings = {
      ["rust-analyzer"] = {
        check = { command = "clippy" },
        completion = { callable = { snippets = "none" } },
        diagnostics = { styleLints = { enable = true } },
      },
    },
  }
end)
