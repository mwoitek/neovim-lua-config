local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("godot", function()
  local common_setup = require "lsp.common_setup"
  local port = 6005
  require("lspconfig").gdscript.setup {
    cmd = vim.lsp.rpc.connect("127.0.0.1", port),
    capabilities = common_setup.capabilities,
    on_attach = function(client, bufnr)
      common_setup.on_attach(client, bufnr)
      client.server_capabilities.completionProvider.triggerCharacters = { "." }
    end,
  }
end)
