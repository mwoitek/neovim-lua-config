local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

local prettier_config = {
  formatCommand = "prettier --stdin --stdin-filepath ${INPUT}",
  formatStdin = true,
  rootMarkers = { ".prettierrc" },
}

local function curr_dir() return debug.getinfo(1, "S").source:match "@(.*)/" end
local styler_script = string.format("%s/styler.R", curr_dir())
local styler_config = {
  formatCommand = string.format("Rscript --no-save --no-restore %s -", styler_script),
  formatStdin = true,
}

run_with_external_dependency("efm-langserver", function()
  local common_setup = require "lsp.common_setup"
  require("lspconfig").efm.setup {
    on_attach = common_setup.on_attach,
    init_options = {
      documentFormatting = true,
      documentRangeFormatting = false,
      hover = false,
      documentSymbol = false,
      codeAction = false,
      completion = false,
    },
    filetypes = {
      "css",
      "fortran",
      "gdscript",
      "html",
      "javascript",
      "json",
      "lua",
      "python",
      "r",
      -- "rmd",
      "sh",
      "sql",
    },
    settings = {
      lintDebounce = "1s",
      rootMarkers = { ".git/" },
      languages = {
        css = { prettier_config },
        fortran = {
          {
            formatCommand = "fprettify --silent --stdout -i 2 -l 80 -w 4 -",
            formatStdin = true,
          },
        },
        gdscript = {
          {
            formatCommand = "gdformat -",
            formatStdin = true,
            rootMarkers = { "project.godot" },
          },
        },
        html = { prettier_config },
        javascript = { prettier_config },
        json = { prettier_config },
        lua = {
          {
            formatCommand = "stylua --color Never -s -",
            formatStdin = true,
            rootMarkers = { "stylua.toml", ".stylua.toml" },
          },
          {
            lintSource = "luacheck",
            lintCommand = "luacheck --codes --no-color --quiet -",
            lintStdin = true,
            lintFormats = { "%.%#:%l:%c: (%t%n) %m" },
            lintIgnoreExitCode = true,
            rootMarkers = { ".luacheckrc" },
          },
        },
        python = {
          {
            formatCommand = "ruff check --fix-only --no-cache --stdin-filename '${INPUT}'",
            formatStdin = true,
            rootMarkers = { "pyproject.toml" },
          },
          {
            formatCommand = "ruff format --no-cache --stdin-filename '${INPUT}'",
            formatStdin = true,
            rootMarkers = { "pyproject.toml" },
          },
          {
            lintSource = "ruff",
            lintCommand = "ruff check --stdin-filename '${INPUT}'",
            lintStdin = true,
            lintFormats = { "%.%#:%l:%c: %m" },
            lintSeverity = 4,
            lintIgnoreExitCode = true,
            rootMarkers = { "pyproject.toml" },
          },
        },
        r = { styler_config },
        rmd = { styler_config },
        sh = {
          {
            formatCommand = "shfmt -i 2 -filename ${INPUT} -",
            formatStdin = true,
          },
        },
        sql = {
          {
            formatCommand = "sqlfluff fix --disable-progress-bar -f -n -",
            formatStdin = true,
            rootMarkers = { ".sqlfluff" },
          },
        },
      },
    },
  }
end)
