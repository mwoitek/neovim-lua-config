local common_setup = require "lsp.common_setup"
require("lspconfig").julials.setup {
  capabilities = common_setup.capabilities,
  on_attach = function(client, bufnr)
    client.server_capabilities.codeActionProvider = false
    common_setup.on_attach(client, bufnr)
  end,
  handlers = {
    ["textDocument/formatting"] = common_setup.formatting_handler_with_diffing,
  },
}
