local common_setup = require "lsp.common_setup"
require("lspconfig").r_language_server.setup {
  capabilities = common_setup.capabilities,
  on_attach = function(client, bufnr)
    client.server_capabilities.documentFormattingProvider = false
    client.server_capabilities.documentRangeFormattingProvider = false
    common_setup.on_attach(client, bufnr)
  end,
  settings = {
    r = {
      lsp = {
        diagnostics = true,
        rich_documentation = false,
        snippet_support = false,
        max_completions = 6,
      },
    },
  },
}
