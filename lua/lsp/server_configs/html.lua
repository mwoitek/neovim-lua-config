local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("vscode-html-language-server", function()
  local common_setup = require "lsp.common_setup"

  local capabilities = common_setup.capabilities
  capabilities.textDocument.completion.completionItem.snippetSupport = true

  require("lspconfig").html.setup {
    capabilities = capabilities,
    on_attach = common_setup.on_attach,
    init_options = {
      configurationSection = { "html", "css", "javascript" },
      embeddedLanguages = { css = true, javascript = true },
      provideFormatter = false,
    },
  }
end)
