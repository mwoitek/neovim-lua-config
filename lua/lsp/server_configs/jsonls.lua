local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("vscode-json-language-server", function()
  local common_setup = require "lsp.common_setup"
  require("lspconfig").jsonls.setup {
    capabilities = common_setup.capabilities,
    on_attach = common_setup.on_attach,
    init_options = { provideFormatter = false },
    settings = {
      json = {
        schemas = require("schemastore").json.schemas(),
        validate = { enable = true },
      },
    },
  }
end)
