local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("clangd", function()
  local common_setup = require "lsp.common_setup"

  local capabilities = common_setup.capabilities
  capabilities.offsetEncoding = { "utf-16" }

  require("lspconfig").clangd.setup {
    cmd = { "clangd", "--clang-tidy" },
    filetypes = { "c", "cpp" },
    capabilities = capabilities,
    on_attach = common_setup.on_attach,
  }
end)
