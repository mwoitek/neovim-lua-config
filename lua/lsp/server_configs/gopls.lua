local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("gopls", function()
  local common_setup = require "lsp.common_setup"

  require("lspconfig").gopls.setup {
    capabilities = common_setup.capabilities,
    on_attach = common_setup.on_attach,
    settings = {
      gopls = {
        analyses = {
          fieldalignment = true,
          nilness = true,
          shadow = true,
          unusedparams = true,
          unusedvariable = true,
          unusedwrite = true,
          useany = true,
        },
        gofumpt = vim.fn.executable "gofumpt" == 1,
        staticcheck = vim.fn.executable "staticcheck" == 1,
      },
    },
  }
end)
