local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("texlab", function()
  local common_setup = require "lsp.common_setup"
  require("lspconfig").texlab.setup {
    capabilities = common_setup.capabilities,
    on_attach = function(client, bufnr)
      client.server_capabilities.documentFormattingProvider = false
      client.server_capabilities.documentRangeFormattingProvider = false
      common_setup.on_attach(client, bufnr)
    end,
    settings = {
      texlab = {
        diagnostics = {
          ignoredPatterns = { "Package rerunfilecheck Warning" },
        },
      },
    },
  }
end)
