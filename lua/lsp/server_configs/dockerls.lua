local dependencies = require "utils.dependencies"
local run_with_external_dependency = dependencies.run_with_external_dependency

run_with_external_dependency("docker-langserver", function()
  local common_setup = require "lsp.common_setup"
  require("lspconfig").dockerls.setup {
    capabilities = common_setup.capabilities,
    on_attach = common_setup.on_attach,
  }
end)
