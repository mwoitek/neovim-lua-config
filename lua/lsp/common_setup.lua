-- vim: set foldmethod=marker:
---@diagnostic disable: need-check-nil

local M = {}
M.capabilities = require("cmp_nvim_lsp").default_capabilities()

-- Diagnostics {{{
local DIAGNOSTICS_ENABLED = false

---@param bufnr number
local toggle_diagnostics = function(bufnr)
  local new_status = not vim.diagnostic.is_enabled { bufnr = bufnr }
  vim.diagnostic.enable(new_status, { bufnr = bufnr })
  local msg = string.format("%s diagnostics", new_status and "Show" or "Hide")
  vim.notify(msg, vim.log.levels.INFO)
end

---@param client vim.lsp.Client
---@param bufnr number
M.on_attach_diagnostics = function(client, bufnr)
  if not client.supports_method "textDocument/publishDiagnostics" then return end

  if vim.b[bufnr].configured_diagnostics then return end
  vim.b[bufnr].configured_diagnostics = true

  vim.diagnostic.enable(DIAGNOSTICS_ENABLED, { bufnr = bufnr })
  vim.keymap.set("n", "<leader>ds", function() toggle_diagnostics(bufnr) end, { buffer = bufnr })

  vim.keymap.set(
    "n",
    "[d",
    function() vim.diagnostic.goto_prev { float = true } end,
    { buffer = bufnr }
  )
  vim.keymap.set(
    "n",
    "]d",
    function() vim.diagnostic.goto_next { float = true } end,
    { buffer = bufnr }
  )

  vim.keymap.set(
    "n",
    "<space>cx",
    function() require("telescope.builtin").diagnostics { bufnr = bufnr } end,
    { buffer = bufnr }
  )
  vim.keymap.set(
    "n",
    "<space>cX",
    function() require("telescope.builtin").diagnostics() end,
    { buffer = bufnr }
  )
end
-- }}}

-- Formatting {{{
-- Handlers {{{
---@param result lsp.TextEdit[]?
---@param ctx lsp.HandlerContext
local simple_formatting_handler = function(_, result, ctx)
  if not result then return end
  local client = assert(vim.lsp.get_client_by_id(ctx.client_id))
  local view = vim.fn.winsaveview()
  vim.lsp.util.apply_text_edits(result, ctx.bufnr, client.offset_encoding)
  if vim.b[ctx.bufnr].format_and_save then vim.cmd "noautocmd update" end
  vim.fn.winrestview(view)
end

vim.lsp.handlers["textDocument/formatting"] = simple_formatting_handler
vim.lsp.handlers["textDocument/rangeFormatting"] = simple_formatting_handler

---@param bufnr? number
---@return string
local get_buffer_content = function(bufnr)
  bufnr = bufnr or 0
  local line_count = vim.api.nvim_buf_line_count(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, line_count, true)
  return vim.iter(lines):join "\n"
end

---@param result lsp.TextEdit[]
---@param orig_code string
---@return lsp.TextEdit[]
local split_format_result = function(result, orig_code)
  if #result ~= 1 then return result end
  local new_code = result[1].newText
  local new_code_lines = vim.split(new_code, "\n")
  ---@type lsp.TextEdit[]
  local new_result = {}
  vim.diff(orig_code, new_code, {
    ignore_blank_lines = false,
    ignore_cr_at_eol = true,
    ignore_whitespace = false,
    ignore_whitespace_change = false,
    ignore_whitespace_change_at_eol = false,
    on_hunk = function(start_orig, count_orig, start_new, count_new)
      ---@type lsp.Range
      local range = {
        start = { line = start_orig - 1, character = 0 },
        ["end"] = { line = start_orig + count_orig - 1, character = 0 },
      }
      ---@type string[]
      local new_text_lines = {}
      for i = start_new, start_new + count_new - 1 do
        table.insert(new_text_lines, new_code_lines[i])
      end
      table.insert(new_text_lines, "")
      local new_text = vim.iter(new_text_lines):join "\n"
      ---@type lsp.TextEdit
      local text_edit = { range = range, newText = new_text }
      table.insert(new_result, text_edit)
      return 0
    end,
  })
  return new_result
end

-- This handler is for servers that respond to a textDocument/formatting
-- request with a single TextEdit that replaces the whole file. I hate this
-- behavior, since it fucks with Neovim's changelist. This function inspects
-- the server's response to figure out what has actually changed. Then a new
-- list of TextEdits is produced and applied.
---@param result lsp.TextEdit[]?
---@param ctx lsp.HandlerContext
M.formatting_handler_with_diffing = function(_, result, ctx)
  if not result then return end
  local client = assert(vim.lsp.get_client_by_id(ctx.client_id))
  local orig_code = get_buffer_content(ctx.bufnr)
  local new_result = split_format_result(result, orig_code)
  if vim.tbl_isempty(new_result) then return end
  local view = vim.fn.winsaveview()
  vim.lsp.util.apply_text_edits(new_result, ctx.bufnr, client.offset_encoding)
  if vim.b[ctx.bufnr].format_and_save then vim.cmd "noautocmd update" end
  vim.fn.winrestview(view)
end
-- }}}

---@param bufnr number
---@param save? boolean
local buf_format = function(bufnr, save)
  vim.b[bufnr].format_and_save = save
  vim.lsp.buf.format {
    bufnr = bufnr,
    async = true,
    formatting_options = { tabSize = vim.lsp.util.get_effective_tabstop(bufnr) },
    timeout_ms = 15000,
  }
end

---@param client vim.lsp.Client
---@param bufnr number
M.on_attach_format = function(client, bufnr)
  local capabilities = client.server_capabilities
  if capabilities.documentFormattingProvider then
    vim.keymap.set("n", "<space>cf", function() buf_format(bufnr) end, { buffer = bufnr })
    vim.api.nvim_create_autocmd("BufWritePost", {
      group = vim.api.nvim_create_augroup("LspFormatOnSave", { clear = false }),
      buffer = bufnr,
      callback = function() buf_format(bufnr, true) end,
    })
  end
  if capabilities.documentRangeFormattingProvider then
    vim.keymap.set("v", "<space>cf", function() buf_format(bufnr) end, { buffer = bufnr })
  end
end
-- }}}

-- on_attach {{{
---@param client vim.lsp.Client
---@param bufnr number
M.on_attach = function(client, bufnr)
  M.on_attach_diagnostics(client, bufnr)
  M.on_attach_format(client, bufnr)

  local capabilities = client.server_capabilities

  if capabilities.codeActionProvider then
    require("lsp.code_actions").create_count_autocmd(bufnr)
    vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, { buffer = bufnr })
  end

  if capabilities.definitionProvider then
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = bufnr })
  end

  if capabilities.implementationProvider then
    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, { buffer = bufnr })
  end

  if capabilities.referencesProvider then
    vim.keymap.set(
      "n",
      "<leader>tr",
      function() require("telescope.builtin").lsp_references() end,
      { buffer = bufnr }
    )
  end

  if capabilities.renameProvider then
    vim.keymap.set("n", "<space>cr", vim.lsp.buf.rename, { buffer = bufnr })
  end
end
-- }}}

return M
