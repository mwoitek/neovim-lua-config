local function get_line_context(bufnr)
  bufnr = bufnr or 0
  local line = vim.api.nvim_win_get_cursor(0)[1]
  return { diagnostics = vim.diagnostic.get(bufnr, { lnum = line }) }
end

local function get_request_params(bufnr)
  local params = vim.lsp.util.make_range_params(0)
  params.context = get_line_context(bufnr)
  return params
end

local function set_action_count(bufnr)
  local action_count = 0
  local params = get_request_params(bufnr)
  vim.lsp.buf_request_all(bufnr, "textDocument/codeAction", params, function(results)
    for _, result in pairs(results) do
      if result and not result.error and result.result then
        action_count = action_count + #result.result
      end
    end
    vim.b[bufnr].action_count = action_count > 0 and action_count or nil
  end)
end

local M = {}

function M.create_count_autocmd(bufnr)
  local group_id = vim.api.nvim_create_augroup("CodeActionGroup", { clear = false })
  local autocmds = vim.api.nvim_get_autocmds { group = group_id, buffer = bufnr }
  if #autocmds > 0 then return end
  vim.api.nvim_create_autocmd("CursorHold", {
    group = group_id,
    buffer = bufnr,
    callback = function() set_action_count(bufnr) end,
  })
end

return M
