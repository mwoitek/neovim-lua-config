local servers = {
  "basedpyright",
  "bashls",
  "clangd",
  "cssls",
  "dockerls",
  "efm-langserver",
  "emmet_ls",
  "fortls",
  "gdscript",
  "gopls",
  "html",
  "jsonls",
  "julials",
  "lua-language-server",
  "r_language_server",
  "rust-analyzer",
  "texlab",
  "ts_ls",
}
local add_prefix_and_load = require("utils.load_files").add_prefix_and_load
add_prefix_and_load(servers, "lsp.server_configs.")
