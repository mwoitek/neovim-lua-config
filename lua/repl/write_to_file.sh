#!/bin/bash

file_content="$1"
[[ -z "$file_content" ]] && exit 1

out_file="$2"
[[ -z "$out_file" ]] && exit 2

printf '%s' "$file_content" >"$out_file"
