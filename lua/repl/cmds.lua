-- vim: set foldmethod=marker:
---@diagnostic disable: duplicate-doc-alias, duplicate-doc-field, duplicate-set-field

local M = {}

local print_err = require("utils.error_handlers").print_err

-- Helper functions {{{
---@param s any
---@return boolean
local is_non_empty_string = function(s) return type(s) == "string" and s:find "^%s*$" == nil end

---@param cmd string
local send_cmd = function(cmd) vim.fn["slime#send"](vim.trim(cmd) .. "\n") end
-- }}}

-- Send generic command {{{
---@param cmd string?
M.send_cmd = function(cmd)
  if is_non_empty_string(cmd) then
    ---@cast cmd string
    send_cmd(cmd)
    return
  end
  vim.ui.input({ prompt = "Enter command to send: " }, function(input)
    if is_non_empty_string(input) then send_cmd(input) end
  end)
end
-- }}}

-- Commands class {{{
---@alias cmd_func fun(modifier: boolean?): string
---@alias cmd_dict table<string, string | cmd_func>

---@class Commands
---@field dict cmd_dict
---@field label string
---@field takes_input boolean?
local Commands = {}
Commands.__index = Commands

---@param opts { dict: cmd_dict, label: string, takes_input: boolean? }
---@return Commands
function Commands:new(opts) return setmetatable(opts, self) end

---@param modifier boolean?
---@return string
function Commands:get(modifier)
  local filetype = assert(vim.bo.filetype, "No filetype for the current buffer")
  local cmd = assert(
    self.dict[filetype],
    string.format("No %s command for this filetype: %s", self.label, filetype)
  )
  if type(cmd) == "function" then cmd = cmd(modifier) end
  return cmd
end

---@param input string?
---@param modifier boolean?
---@return string
function Commands:build(input, modifier)
  local cmd = self:get(modifier)
  if not self.takes_input then return cmd end
  if is_non_empty_string(input) then return cmd:format(input) end
  error "Input has to be a non-empty string"
end

---@param input string?
---@param modifier boolean?
function Commands:_send(input, modifier)
  local cmd = self:build(input, modifier)
  send_cmd(cmd)
end

---@param input string?
---@param modifier boolean?
function Commands:_send_normal(input, modifier)
  if not self.takes_input or is_non_empty_string(input) then
    self:_send(input, modifier)
    return
  end
  local prompt = string.format("Enter input to %s command: ", self.label)
  vim.ui.input({ prompt = prompt }, function(input_)
    if is_non_empty_string(input_) then self:_send(input_, modifier) end
  end)
end

---@param modifier boolean?
function Commands:_send_visual(modifier)
  local selection = require "utils.visual_selection"
  local range = selection.get_range()
  local text = selection.get_text(0, range)
  local input = text[1]
  self:_send_normal(input, modifier)
end

---@param input string?
---@param modifier boolean?
function Commands:send(input, modifier)
  local mode = vim.api.nvim_get_mode().mode
  if mode == "n" then
    self:_send_normal(input, modifier)
  elseif mode == "v" then
    self:_send_visual(modifier)
  else
    error "Invalid mode"
  end
end
-- }}}

-- Command definitions {{{
---@type Commands[]
local cmds = {}

-- Help {{{
table.insert(
  cmds,
  Commands:new {
    dict = {
      julia = "@doc %s",
      python = function(modifier) return modifier and "??%s" or "help(%s)" end,
      r = [[help("%s")]],
    },
    label = "help",
    takes_input = true,
  }
)
-- }}}

-- Source file {{{
table.insert(
  cmds,
  Commands:new {
    dict = {
      julia = function(quiet) return quiet and [[include("%s");]] or [[include("%s")]] end,
      python = function(quiet) return quiet and "%%quiet_run %s" or "%%run -i %s" end,
      r = [[source("%s")]],
    },
    label = "source",
    takes_input = true,
  }
)
-- }}}

-- Clear {{{
table.insert(
  cmds,
  Commands:new {
    dict = {
      julia = [[empty!(Out); print("\033c")]],
      python = "clear",
      r = [[cat("\033c")]],
    },
    label = "clear",
  }
)
-- }}}

-- Exit {{{
table.insert(
  cmds,
  Commands:new {
    dict = {
      julia = "exit()",
      python = "exit",
      r = [[q(save = "no")]],
    },
    label = "exit",
  }
)
-- }}}

-- Define module functions {{{
for _, cmd in ipairs(cmds) do
  local label = cmd.label:lower()
  local func_name = string.format("send_%s_cmd", label)
  if cmd.takes_input then
    M[func_name] = function(input, modifier) xpcall(cmd.send, print_err, cmd, input, modifier) end
  else
    M[func_name] = function(modifier) xpcall(cmd.send, print_err, cmd, nil, modifier) end
  end
end
-- }}}
-- }}}

-- REPL {{{
---@type table<string, string>
local BASIC_CMDS = {
  julia = "julia --quiet",
  python = "ipython --profile=nvim",
  r = "R --no-restore --no-save --quiet",
}
---@type table<string, string[]>
local ENV_VARS = {
  julia = { "JULIA_LOAD_PATH", "JULIA_PROJECT" },
  python = { "PYTHONPATH" },
}

---@param var_names string[]
---@return string?
local collect_env_vars = function(var_names)
  ---@type string[]
  local assignments = {}
  for _, name in ipairs(var_names) do
    local val = os.getenv(name)
    if val then table.insert(assignments, string.format("%s=%s", name, val)) end
  end
  if #assignments == 0 then return end
  return table.concat(assignments, " ")
end

---@return string
M.get_repl_cmd = function()
  local filetype = vim.bo.filetype
  if not filetype then error "No filetype for the current buffer" end
  local basic_cmd = BASIC_CMDS[filetype]
  if not basic_cmd then
    local msg = string.format("No REPL command for this filetype: %s", filetype)
    error(msg)
  end
  local main_cmd = vim.split(basic_cmd, " ")[1]
  if vim.fn.executable(main_cmd) == 0 then
    local msg = string.format("REPL command is defined but not available: %s", main_cmd)
    error(msg)
  end
  local var_names = ENV_VARS[filetype]
  if not var_names then return basic_cmd end
  local env_vars = collect_env_vars(var_names)
  return env_vars and env_vars .. " " .. basic_cmd or basic_cmd
end
-- }}}

return M
