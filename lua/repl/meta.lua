---@meta

---@alias CellRange { first: integer, last: integer }
---@alias CellType "code" | "md"
---@alias Direction "up" | "down"
