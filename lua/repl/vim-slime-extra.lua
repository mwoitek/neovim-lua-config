-- vim: set foldmethod=marker:

local M = {}

local cmds = require "repl.cmds"
local overrides = require "repl.vim-slime-overrides"
local range = require "repl.send_range"

-- Register override functions {{{
for _, file_type in ipairs { "julia", "python", "r" } do
  overrides.register_override(file_type)
end
-- }}}

-- Send generic command {{{
vim.api.nvim_create_user_command(
  "ReplSendCommand",
  function(opts) cmds.send_cmd(opts.args) end,
  { nargs = "?" }
)
-- }}}

-- Send command to show help {{{
vim.api.nvim_create_user_command(
  "ReplShowHelp",
  function(opts) cmds.send_help_cmd(opts.args, opts.bang) end,
  { bang = true, nargs = "?" }
)
-- }}}

-- Source current file {{{
vim.api.nvim_create_user_command("ReplSourceCurrFile", function(opts)
  local curr_file = vim.api.nvim_buf_get_name(0)
  cmds.send_source_cmd(curr_file, opts.bang)
end, { bang = true })
-- }}}

-- Send command to clear terminal {{{
vim.api.nvim_create_user_command("ReplClear", function() cmds.send_clear_cmd() end, {})
-- }}}

-- Send command to exit REPL {{{
vim.api.nvim_create_user_command("ReplExit", function()
  cmds.send_exit_cmd()
  vim.b.slime_config = nil
end, {})
-- }}}

-- Send all lines above {{{
vim.api.nvim_create_user_command(
  "ReplSendAllAbove",
  function(opts) range.send_all_above(opts.bang) end,
  { bang = true }
)
-- }}}

-- Send all lines below {{{
vim.api.nvim_create_user_command(
  "ReplSendAllBelow",
  function(opts) range.send_all_below(opts.bang) end,
  { bang = true }
)
-- }}}

-- Add default keybindings {{{
local add_common_keybindings = function()
  vim.keymap.set("n", "<leader>c", "<cmd>ReplSendCommand<cr>", { buffer = 0 })
  vim.keymap.set({ "n", "v" }, "<space>mH", "<cmd>ReplShowHelp!<cr>", { buffer = 0 })
  vim.keymap.set({ "n", "v" }, "<space>mh", "<cmd>ReplShowHelp<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>mF", "<cmd>ReplSourceCurrFile!<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>mf", "<cmd>ReplSourceCurrFile<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>md", "<cmd>ReplClear<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>mx", "<cmd>ReplExit<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>mA", "<cmd>ReplSendAllAbove!<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>ma", "<cmd>ReplSendAllAbove<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>mB", "<cmd>ReplSendAllBelow!<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>mb", "<cmd>ReplSendAllBelow<cr>", { buffer = 0 })
end

---@type table<string, fun()>
local filetype_keybindings = {
  python = function()
    vim.keymap.set("n", "<space>mR", function()
      vim.cmd "ReplSendCommand %reset -f"
      vim.cmd "ReplClear"
    end, { buffer = 0 })
    vim.keymap.set("n", "<space>mp", [[<cmd>ReplSendCommand plt.close("all")<cr>]], { buffer = 0 })
    vim.keymap.set("n", "<space>mw", "<cmd>ReplSendCommand %whos<cr>", { buffer = 0 })
  end,
}

local add_filetype_keybindings = function()
  local kb_func = filetype_keybindings[vim.bo.filetype]
  if type(kb_func) == "function" then kb_func() end
end

M.setup = function()
  add_common_keybindings()
  add_filetype_keybindings()
end
-- }}}

return M
