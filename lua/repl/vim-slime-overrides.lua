-- vim: set foldmethod=marker:

local M = {}

local process = require("repl.text_processing").process

-- Define override functions {{{
---@param file_type string?
---@return fun(text: string): string
local build_escape_text = function(file_type)
  return function(text)
    local new_text = process(text)
    if not new_text then return "" end
    if type(file_type) == "string" then new_text = vim.fn["_EscapeText_" .. file_type](new_text) end
    return new_text
  end
end

M.escape_text = build_escape_text()
M.escape_text_python = build_escape_text "python"
-- }}}

-- Register override functions {{{
---@param file_type string
M.register_override = function(file_type)
  local func_name = string.format("escape_text_%s", file_type)
  if type(M[func_name]) ~= "function" then func_name = "escape_text" end
  local vimscript_func = [[
function! SlimeOverride_EscapeText_%s(text)
  return v:lua.require'repl.vim-slime-overrides'.%s(a:text)
endfunction
]]
  vimscript_func = vimscript_func:format(file_type, func_name)
  vim.cmd(vimscript_func)
end
-- }}}

return M
