-- vim: set foldmethod=marker:

local M = {}

-- Insert cell delimiters {{{
---@param line string
---@param count integer?
local insert_lines = function(line, count)
  count = count or 1
  local curr_line_num = vim.api.nvim_win_get_cursor(0)[1]
  local curr_line = vim.fn.getline(curr_line_num)
  ---@type string[]
  local lines = {}
  if not curr_line:match "^%s*$" then table.insert(lines, curr_line) end
  for _ = 1, count do
    table.insert(lines, line)
    table.insert(lines, "")
  end
  local mode = vim.api.nvim_get_mode().mode
  if mode == "n" then table.remove(lines) end
  vim.api.nvim_buf_set_lines(0, curr_line_num - 1, curr_line_num, true, lines)
  if mode == "i" then
    local new_pos = { 0, 0 }
    new_pos[1] = curr_line_num + #lines - 1
    new_pos[2] = vim.fn.col { new_pos[1], "$" }
    vim.api.nvim_win_set_cursor(0, new_pos)
  end
end

---@param cell_type CellType?
---@param count integer?
M.insert_cell_delimiter = function(cell_type, count)
  cell_type = cell_type or "code"
  local subs = cell_type == "code" and "%%" or "%% [markdown]"
  local line = vim.bo.commentstring:format(subs)
  insert_lines(line, count)
end
-- }}}

-- Get pattern that matches a cell delimiter {{{
---@param cell_type CellType?
---@return string
local get_cell_delimiter_pattern = function(cell_type)
  local pattern = [[^\s*]] .. vim.bo.commentstring:format("%%"):gsub("%s+", [[\s*]])
  if not cell_type then return pattern end
  local md_pattern = ".*\\[markdown\\]"
  return cell_type == "md" and pattern .. md_pattern
    or string.format([[%s\(%s\)\@!]], pattern, md_pattern)
end
-- }}}

-- Find lines that match a pattern {{{
vim.cmd [[
function! CellsFindMatchingLines(pattern, first_line, last_line)
  let view = winsaveview()
  let line_nums = []
  execute 'sil! keepj keepp ' . a:first_line . ',' . a:last_line . 'g/' .
          \ a:pattern . "/call add(line_nums, line('.'))"
  call winrestview(view)
  return line_nums
endfunction
]]

---@param pattern string
---@param first_line integer?
---@param last_line integer?
---@return integer[]?
local find_matching_lines = function(pattern, first_line, last_line)
  first_line = first_line or 1
  last_line = last_line or vim.api.nvim_buf_line_count(0)
  ---@type integer[]
  local line_nums = vim.fn.CellsFindMatchingLines(pattern, first_line, last_line)
  if #line_nums == 0 then return end
  return line_nums
end
-- }}}

-- Validate line range {{{
---@param first_line integer
---@param last_line integer
---@return boolean
local is_valid_range = function(first_line, last_line)
  return first_line <= last_line and first_line >= 1 and last_line <= vim.api.nvim_buf_line_count(0)
end
-- }}}

-- Find n-th delimiter {{{
---@param direction Direction
---@param cell_type CellType?
---@param n integer?
---@param opts { allow_less: boolean?, include_curr: boolean? }?
---@return integer?
local find_nth_delim = function(direction, cell_type, n, opts)
  n = n or 1
  local default_opts = { allow_less = false, include_curr = false }
  opts = vim.tbl_extend("force", default_opts, opts or {})
  local curr_line = vim.api.nvim_win_get_cursor(0)[1]
  local first_line, last_line
  if direction == "up" then
    first_line = 1
    last_line = opts.include_curr and curr_line or curr_line - 1
  else
    first_line = curr_line + 1
    last_line = vim.api.nvim_buf_line_count(0)
  end
  if not is_valid_range(first_line, last_line) then return end
  local delim_pattern = get_cell_delimiter_pattern(cell_type)
  local line_nums = find_matching_lines(delim_pattern, first_line, last_line)
  if not line_nums then return end
  local line_count = #line_nums
  if line_count < n then
    if not opts.allow_less then return end
    return direction == "up" and line_nums[1] or line_nums[line_count]
  end
  return direction == "up" and line_nums[line_count - n + 1] or line_nums[n]
end
-- }}}

-- Jump to cells {{{
---@param direction Direction
---@param cell_type CellType?
---@param count integer?
local jump_to_cell = function(direction, cell_type, count)
  local delim_line = find_nth_delim(direction, cell_type, count, { allow_less = true })
  if not delim_line then return end
  vim.api.nvim_win_set_cursor(0, { delim_line, 0 })
end

---@param count integer?
M.jump_to_prev = function(count) jump_to_cell("up", nil, count) end

---@param count integer?
M.jump_to_next = function(count) jump_to_cell("down", nil, count) end

---@param count integer?
M.jump_to_prev_code = function(count) jump_to_cell("up", "code", count) end

---@param count integer?
M.jump_to_next_code = function(count) jump_to_cell("down", "code", count) end

---@param count integer?
M.jump_to_prev_md = function(count) jump_to_cell("up", "md", count) end

---@param count integer?
M.jump_to_next_md = function(count) jump_to_cell("down", "md", count) end
-- }}}

-- Remove trailing blanks from range {{{
---@param cell_range CellRange
---@return CellRange
local remove_trailing_blanks = function(cell_range)
  local first_line, last_line = cell_range.first, cell_range.last
  local lines = vim.api.nvim_buf_get_lines(0, first_line - 1, last_line, true)
  for i, line in vim.iter(lines):rev():enumerate() do
    if not line:match "^%s*$" then
      last_line = last_line + i - #lines
      break
    end
  end
  return { first = first_line, last = last_line }
end
-- }}}

-- Get cell range {{{
---@param inner boolean?
---@return CellRange?
local get_cell_range = function(inner)
  local first_line = find_nth_delim("up", nil, 1, { include_curr = true })
  first_line = first_line or find_nth_delim "down"
  if not first_line then return end
  local n = first_line > vim.api.nvim_win_get_cursor(0)[1] and 2 or 1
  local last_line = find_nth_delim("down", nil, n)
  if not last_line or first_line == last_line then
    last_line = vim.api.nvim_buf_line_count(0)
  else
    last_line = last_line - 1
  end
  local cell_range = { first = first_line, last = last_line }
  if inner then
    cell_range.first = cell_range.first + 1
    cell_range = remove_trailing_blanks(cell_range)
  end
  if not is_valid_range(cell_range.first, cell_range.last) then return end
  return cell_range
end
-- }}}

-- Cell textobject {{{
---@param inner boolean?
M.cell_textobject = function(inner)
  local mode = vim.api.nvim_get_mode().mode
  if mode:lower() ~= "v" then return end
  local cell_range = get_cell_range(inner)
  if not cell_range then return end
  vim.cmd("normal! " .. mode)
  vim.api.nvim_win_set_cursor(0, { cell_range.first, 0 })
  vim.cmd "normal! v"
  local last_col = vim.fn.col { cell_range.last, "$" } - 1
  if inner then last_col = last_col - 1 end
  vim.api.nvim_win_set_cursor(0, { cell_range.last, last_col })
end
-- }}}

-- Move cells {{{
---@param direction Direction
---@param n integer?
---@return integer?
local find_destination_line = function(direction, n)
  n = n or 1
  if direction == "up" then n = n + 1 end
  local delim_line = find_nth_delim(direction, nil, n, { include_curr = true })
  if not delim_line then return end
  if direction == "up" then return delim_line - 1 end
  local view = vim.fn.winsaveview()
  vim.api.nvim_win_set_cursor(0, { delim_line, 0 })
  local last_line = assert(get_cell_range()).last
  vim.fn.winrestview(view)
  return last_line
end

---@param direction Direction
---@param n integer?
local move_cell = function(direction, n)
  local full_range = get_cell_range()
  if not full_range or full_range.first > vim.api.nvim_win_get_cursor(0)[1] then return end
  local dest_line = find_destination_line(direction, n)
  if not dest_line then return end
  local clean_range = remove_trailing_blanks(full_range)
  local lines = vim.api.nvim_buf_get_lines(0, clean_range.first - 1, clean_range.last, true)
  local cursor_line = dest_line + 1
  if direction == "up" then
    vim.api.nvim_buf_set_lines(0, full_range.first - 1, full_range.last, true, {})
    table.insert(lines, "")
    vim.api.nvim_buf_set_lines(0, dest_line, dest_line, false, lines)
  else
    local pos = #lines + 1
    if dest_line == vim.api.nvim_buf_line_count(0) then
      cursor_line = cursor_line + 1
      pos = 1
    end
    table.insert(lines, pos, "")
    vim.api.nvim_buf_set_lines(0, dest_line, dest_line, false, lines)
    vim.api.nvim_buf_set_lines(0, full_range.first - 1, full_range.last, true, {})
    cursor_line = cursor_line - (full_range.last - full_range.first + 1)
  end
  vim.api.nvim_win_set_cursor(0, { cursor_line, 0 })
end

---@param count integer?
M.move_cell_up = function(count) move_cell("up", count) end

---@param count integer?
M.move_cell_down = function(count) move_cell("down", count) end
-- }}}

-- Fold cells {{{
---@return integer?
local get_first_delimiter = function()
  local cursor = vim.api.nvim_win_get_cursor(0)
  vim.api.nvim_win_set_cursor(0, { 1, 0 })
  ---@type integer
  local line_num = vim.fn.search(get_cell_delimiter_pattern(), "Wcn")
  vim.api.nvim_win_set_cursor(0, cursor)
  if line_num == 0 then return end
  return line_num
end

M.setup_folding = function()
  vim.b.cells_delim_line = get_first_delimiter()
  vim.b.cells_delim_pat = get_cell_delimiter_pattern()
  local winid = vim.api.nvim_get_current_win()
  vim.wo[winid][0].foldexpr = "v:lua.require'repl.code_cells_lib'.foldexpr()"
  vim.wo[winid][0].foldmethod = "expr"
  for _, k in ipairs { "X", "x" } do
    local lhs = "z" .. k
    vim.keymap.set("n", lhs, function()
      vim.b.cells_delim_line = get_first_delimiter()
      vim.cmd("normal! " .. lhs)
    end, { buffer = 0 })
  end
end

---@return string
M.foldexpr = function()
  local curr_line = vim.fn.getline(vim.v.lnum)
  if curr_line:find "^%s*$" then return "-1" end
  if vim.fn.match(curr_line, vim.b.cells_delim_pat) ~= -1 then return "0" end
  local extra_level = (vim.b.cells_delim_line and vim.v.lnum > vim.b.cells_delim_line) and 1 or 0
  local fold_level = math.floor(vim.fn.indent(vim.v.lnum) / vim.bo.shiftwidth) + extra_level
  return fold_level <= vim.wo.foldnestmax and tostring(fold_level) or "-1"
end
-- }}}

return M
