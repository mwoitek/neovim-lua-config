-- vim: foldmethod=marker:

local M = {}

local dedent = require("plenary.strings").dedent

-- Filter out empty lines and comments {{{
-- Filter for a single line {{{
---@param line string
---@param comment string
---@param allow_magic boolean
---@return boolean
local filter_line = function(line, comment, allow_magic)
  if line:find "^%s*$" then return false end
  local comment_pattern = "^%s*" .. comment .. "%s*(.*)"
  local comment_match = line:match(comment_pattern)
  if not comment_match then return true end
  return allow_magic and comment_match:find "^%%%%?%a+" ~= nil
end

---@return fun(line: string): boolean
local get_filter = function()
  local comment = vim.trim(vim.bo.commentstring:format "")
  local allow_magic = vim.bo.filetype == "python"
  return function(line) return filter_line(line, comment, allow_magic) end
end
-- }}}

-- Apply filter to every line {{{
---@param text string | string[]
---@return string?
M.filter = function(text)
  if type(text) == "string" then text = vim.split(text, "\n") end
  local filtered_text = vim.tbl_filter(get_filter(), text)
  if vim.tbl_isempty(filtered_text) then return end
  return table.concat(filtered_text, "\n") .. "\n"
end
-- }}}
-- }}}

-- Julia: Fix includes with relative paths {{{
---@param text string | string[]
---@return string
local fix_julia_includes = function(text)
  if type(text) == "string" then text = vim.split(text, "\n") end
  local curr_dir = vim.fs.dirname(vim.api.nvim_buf_get_name(0))
  local pattern = [[^(%s*)include%("(.+)"%)]]
  for i = 1, #text do
    text[i] = string.gsub(text[i], pattern, function(c1, c2)
      local path = vim.startswith(c2, ".") and vim.fs.joinpath(curr_dir, c2) or c2
      return string.format([[%sinclude("%s")]], c1, path)
    end)
  end
  return table.concat(text, "\n")
end
-- }}}

-- Process text {{{
---@type table<string, (fun(text: string | string[]): string)[]>
local extra_steps = {
  julia = { fix_julia_includes },
}

---@param text string | string[]
---@return string?
M.process = function(text)
  local new_text = M.filter(text)
  if not new_text then return end
  local extra_funcs = extra_steps[vim.bo.filetype] or {}
  for _, func in ipairs(extra_funcs) do
    new_text = func(new_text)
  end
  return dedent(new_text)
end
-- }}}

return M
