-- vim: set foldmethod=marker:

local M = {}

local Job = require "plenary.job"
local process = require("repl.text_processing").process
---@type fun(input: string?, modifier: boolean?)
local send_source_cmd = require("repl.cmds").send_source_cmd

local WRITE_SCRIPT = vim.api.nvim_get_runtime_file("**/write_to_file.sh", false)[1]

-- Temporary file {{{
---@return string
local get_tmp_file = function()
  local tmp_file = vim.fn.tempname()
  local tmp_dir = vim.fs.dirname(tmp_file)
  local base_name = vim.fs.basename(tmp_file)
  local curr_file = vim.api.nvim_buf_get_name(0)
  local curr_ext = table.remove(vim.split(curr_file, ".", { plain = true }))
  return vim.fs.joinpath(tmp_dir, base_name .. "." .. curr_ext)
end
-- }}}

-- Save code to a file then source it {{{
---@param code string
---@param quiet boolean?
local save_and_source = function(code, quiet)
  local file_path = get_tmp_file()
  Job:new({
    command = WRITE_SCRIPT,
    args = { code, file_path },
    on_exit = function(_, exit_status)
      vim.schedule(function()
        if exit_status ~= 0 then
          vim.notify("Failed to save file to be sourced", vim.log.levels.ERROR)
          return
        end
        send_source_cmd(file_path, quiet)
      end)
    end,
  }):start()
end
-- }}}

-- Send a range of lines {{{
---@param first_line integer?
---@param last_line integer?
---@param quiet boolean?
M.send_line_range = function(first_line, last_line, quiet)
  first_line = first_line or 1
  last_line = last_line or vim.api.nvim_buf_line_count(0)
  local lines = vim.api.nvim_buf_get_lines(0, first_line - 1, last_line, true)
  local processed = process(lines)
  if not processed then
    vim.notify("No line to send", vim.log.levels.INFO)
    return
  end
  save_and_source(processed, quiet)
end
-- }}}

-- Send all lines above {{{
---@param quiet boolean?
M.send_all_above = function(quiet)
  local curr_line = vim.api.nvim_win_get_cursor(0)[1]
  if curr_line == 1 then
    vim.notify("There is no line above", vim.log.levels.INFO)
    return
  end
  M.send_line_range(nil, curr_line - 1, quiet)
end
-- }}}

-- Send all lines below {{{
---@param quiet boolean?
M.send_all_below = function(quiet)
  local curr_line = vim.api.nvim_win_get_cursor(0)[1]
  if curr_line == vim.api.nvim_buf_line_count(0) then
    vim.notify("There is no line below", vim.log.levels.INFO)
    return
  end
  M.send_line_range(curr_line, nil, quiet)
end
-- }}}

return M
