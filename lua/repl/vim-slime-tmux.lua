-- vim: set foldmethod=marker:

local M = {}

local print_err = require("utils.error_handlers").print_err

-- Parameters {{{
local TMUX_SESSION_NAME = "slime"
local TERMINAL_CMD = "wezterm start --class tmux_slime -- %s >/dev/null 2>&1"
local WINDOW_NAME_PREFIX = "repl"
-- }}}

-- Session {{{
---@param session_name string?
---@return boolean
M.has_tmux_session = function(session_name)
  session_name = session_name or TMUX_SESSION_NAME
  local tmux_cmd =
    string.format("tmux list-sessions -F '#S' 2>/dev/null | grep -qx %s", session_name)
  return os.execute(tmux_cmd) == 0
end

---@param session_name string?
M.start_tmux_session = function(session_name)
  session_name = session_name or TMUX_SESSION_NAME
  if M.has_tmux_session(session_name) then
    local msg = string.format("tmux session named %s already exists", session_name)
    vim.notify(msg, vim.log.levels.INFO)
    return
  end
  local tmux_cmd = string.format("tmux new-session -d -n empty -s %s >/dev/null 2>&1", session_name)
  if os.execute(tmux_cmd) == 0 then return end
  error "Failed to start tmux session"
end
-- }}}

-- Window {{{
---@param window_name string
---@param session_name string?
---@return boolean
M.has_tmux_window = function(window_name, session_name)
  session_name = session_name or TMUX_SESSION_NAME
  local tmux_cmd =
    string.format("tmux list-windows -F '#W' -t %s | grep -qx %s", session_name, window_name)
  return os.execute(tmux_cmd) == 0
end

---@param prefix string?
---@return string
local build_window_name = function(prefix)
  prefix = prefix or WINDOW_NAME_PREFIX
  local file_path = vim.api.nvim_buf_get_name(0)
  local file_name = vim.fs.basename(file_path)
  local file_name_no_ext = vim.split(file_name, ".", { plain = true })[1]
  return string.format("%s-%s", prefix, file_name_no_ext)
end

---@param cmd string
---@param window_name string?
---@param session_name string?
M.create_tmux_window = function(cmd, window_name, session_name)
  window_name = window_name or build_window_name()
  session_name = session_name or TMUX_SESSION_NAME
  if M.has_tmux_window(window_name, session_name) then
    local msg =
      string.format("tmux session %s already has a window named %s", session_name, window_name)
    vim.notify(msg, vim.log.levels.INFO)
    return
  end
  local tmux_cmd = string.format(
    "tmux new-window -S -c %s -n %s -t %s '%s' >/dev/null 2>&1",
    vim.fn.getcwd(),
    window_name,
    session_name,
    cmd
  )
  if os.execute(tmux_cmd) == 0 then return end
  error "Failed to create tmux window"
end

---@param window_name string
---@param session_name string?
M.kill_tmux_window = function(window_name, session_name)
  session_name = session_name or TMUX_SESSION_NAME
  if not M.has_tmux_window(window_name, session_name) then return end
  local target_window = string.format("%s:%s", session_name, window_name)
  local tmux_cmd = string.format("tmux kill-window -t %s", target_window)
  os.execute(tmux_cmd)
end
-- }}}

-- Setup tmux {{{
---@param session_name string?
---@param window_name string?
---@return table<string, string>
M.build_slime_config = function(session_name, window_name)
  local repl_cmd = require("repl.cmds").get_repl_cmd()
  session_name = session_name or TMUX_SESSION_NAME
  M.start_tmux_session(session_name)
  window_name = window_name or build_window_name()
  M.create_tmux_window(repl_cmd, window_name, session_name)
  return {
    socket_name = "default",
    target_pane = string.format("%s:%s", session_name, window_name),
  }
end

---@param session_name string?
---@param window_name string?
---@return boolean
M.setup_tmux = function(session_name, window_name)
  local ok, slime_config = xpcall(M.build_slime_config, print_err, session_name, window_name)
  if not ok then return false end
  vim.b.slime_config = slime_config
  M.kill_tmux_window("empty", session_name)
  vim.notify("tmux is setup", vim.log.levels.INFO)
  return true
end
-- }}}

-- Terminal {{{
---@param session_name string?
---@return boolean
M.has_open_terminal = function(session_name)
  session_name = session_name or TMUX_SESSION_NAME
  local term = vim.split(TERMINAL_CMD, " ")[1]
  local pattern = term .. ".+" .. session_name
  local check_cmd = string.format("ps axo command | grep -E '%s' | grep -qv grep", pattern)
  return os.execute(check_cmd) == 0
end

---@param session_name string?
local _open_terminal = function(session_name)
  session_name = session_name or TMUX_SESSION_NAME
  if M.has_open_terminal(session_name) then
    local msg =
      string.format("You already have an open terminal showing tmux session %s", session_name)
    vim.notify(msg, vim.log.levels.INFO)
    return
  end
  if not vim.b.slime_config then error "Open terminal only after tmux is setup" end
  local term = vim.split(TERMINAL_CMD, " ")[1]
  if vim.fn.executable(term) == 0 then
    local msg = string.format("Terminal not available: %s", term)
    error(msg)
  end
  local sh_cmd = string.format("sh -l -c 'tmux attach-session -t %s'", session_name)
  local terminal_cmd = TERMINAL_CMD:format(sh_cmd) .. "&"
  if os.execute(terminal_cmd) == 0 then return end
  error "Failed to open terminal"
end

---@param session_name string?
M.open_terminal = function(session_name)
  local ok, _ = xpcall(_open_terminal, print_err, session_name)
  if not ok then return end
  vim.notify("Terminal is open", vim.log.levels.INFO)
end
-- }}}

-- Full setup {{{
---@param session_name string?
---@param window_name string?
M.start = function(session_name, window_name)
  local is_setup = M.setup_tmux(session_name, window_name)
  if is_setup then M.open_terminal(session_name) end
end
-- }}}

-- Keybindings {{{
---@param mode string | string[]
---@param lhs string
---@param rhs string
---@param opts table?
M.set_slime_keymap = function(mode, lhs, rhs, opts)
  if type(mode) == "string" then mode = { mode } end
  rhs = string.format("%c%c%c%s", 0x80, 253, 83, rhs)
  opts = opts or {}
  for _, m in ipairs(mode) do
    vim.keymap.set(m, lhs, function()
      if not vim.b.slime_config then
        vim.notify("To send code, tmux needs to be setup", vim.log.levels.WARN)
        return
      end
      vim.api.nvim_feedkeys(rhs, m, false)
    end, opts)
  end
end

M.add_keybindings = function()
  vim.keymap.set("n", "<space>mr", M.start, { buffer = 0 })
  M.set_slime_keymap("n", "<space>mc", "SlimeSendCell", { buffer = 0 })
  M.set_slime_keymap("n", "<space>ml", "SlimeLineSend", { buffer = 0 })
  M.set_slime_keymap("n", "gm", "SlimeMotionSend", { buffer = 0 })
  M.set_slime_keymap("x", "<space>ms", "SlimeRegionSend", { buffer = 0 })
end
-- }}}

return M
