-- vim: set foldmethod=marker:

local M = {}

local lib = require "repl.code_cells_lib"

-- Insert cell delimiters {{{
vim.keymap.set("i", "<plug>CellsInsertCode", function() lib.insert_cell_delimiter "code" end, {})
vim.keymap.set("i", "<plug>CellsInsertMd", function() lib.insert_cell_delimiter "md" end, {})

vim.keymap.set(
  "n",
  "<plug>CellsInsertCode",
  function() lib.insert_cell_delimiter("code", vim.v.count1) end,
  {}
)
vim.keymap.set(
  "n",
  "<plug>CellsInsertMd",
  function() lib.insert_cell_delimiter("md", vim.v.count1) end,
  {}
)
-- }}}

-- Jump to cells {{{
vim.api.nvim_create_user_command(
  "CellsJumpToPrev",
  function() lib.jump_to_prev(vim.v.count1) end,
  {}
)
vim.api.nvim_create_user_command(
  "CellsJumpToNext",
  function() lib.jump_to_next(vim.v.count1) end,
  {}
)

vim.api.nvim_create_user_command(
  "CellsJumpToPrevCode",
  function() lib.jump_to_prev_code(vim.v.count1) end,
  {}
)
vim.api.nvim_create_user_command(
  "CellsJumpToNextCode",
  function() lib.jump_to_next_code(vim.v.count1) end,
  {}
)

vim.api.nvim_create_user_command(
  "CellsJumpToPrevMd",
  function() lib.jump_to_prev_md(vim.v.count1) end,
  {}
)
vim.api.nvim_create_user_command(
  "CellsJumpToNextMd",
  function() lib.jump_to_next_md(vim.v.count1) end,
  {}
)
-- }}}

-- Cell textobject {{{
vim.keymap.set("x", "<plug>CellsObjOuter", lib.cell_textobject, {})
vim.keymap.set("x", "<plug>CellsObjInner", function() lib.cell_textobject(true) end, {})
vim.keymap.set("o", "<plug>CellsObjOuter", "<cmd>normal v<plug>CellsObjOuter<cr>", {})
vim.keymap.set("o", "<plug>CellsObjInner", "<cmd>normal v<plug>CellsObjInner<cr>", {})
-- }}}

-- Move cells {{{
vim.api.nvim_create_user_command("CellsMoveUp", function() lib.move_cell_up(vim.v.count1) end, {})
vim.api.nvim_create_user_command(
  "CellsMoveDown",
  function() lib.move_cell_down(vim.v.count1) end,
  {}
)
-- }}}

-- Add default keybindings {{{
local add_default_keybindings = function()
  vim.keymap.set("i", "]c", "<plug>CellsInsertCode", { buffer = 0 })
  vim.keymap.set("i", "]m", "<plug>CellsInsertMd", { buffer = 0 })
  vim.keymap.set("n", "<leader>jc", "<plug>CellsInsertCode", { buffer = 0 })
  vim.keymap.set("n", "<leader>jm", "<plug>CellsInsertMd", { buffer = 0 })
  vim.keymap.set("n", "<space>jn", "<cmd>CellsJumpToNext<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>jp", "<cmd>CellsJumpToPrev<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>jc", "<cmd>CellsJumpToNextCode<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>jC", "<cmd>CellsJumpToPrevCode<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>jm", "<cmd>CellsJumpToNextMd<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>jM", "<cmd>CellsJumpToPrevMd<cr>", { buffer = 0 })
  vim.keymap.set({ "o", "x" }, "aj", "<plug>CellsObjOuter", { buffer = 0 })
  vim.keymap.set({ "o", "x" }, "ij", "<plug>CellsObjInner", { buffer = 0 })
  vim.keymap.set("n", "<space>jd", "<cmd>CellsMoveDown<cr>", { buffer = 0 })
  vim.keymap.set("n", "<space>ju", "<cmd>CellsMoveUp<cr>", { buffer = 0 })
end
-- }}}

-- Setup function {{{
---@param opts { fold: boolean?, keybindings: boolean? }?
M.setup = function(opts)
  local default_opts = { fold = true, keybindings = true }
  opts = vim.tbl_extend("force", default_opts, opts or {})
  if opts.fold then lib.setup_folding() end
  if opts.keybindings then add_default_keybindings() end
end
-- }}}

return M
