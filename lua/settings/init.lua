local settings_files = {
  "basic",
  "folds",
  "diagnostic",
  "providers",
  "builtins",
}
local add_prefix_and_load = require("utils.load_files").add_prefix_and_load
add_prefix_and_load(settings_files, "settings.")
