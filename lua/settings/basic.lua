vim.o.cmdwinheight = 15
vim.o.ruler = false
vim.o.showcmd = false
vim.o.showmode = false
vim.o.wildmode = "longest:full,full"

vim.o.equalalways = true
vim.o.helpheight = 12

vim.o.hlsearch = false
vim.o.ignorecase = true
vim.o.smartcase = true

vim.bo.expandtab = true
vim.bo.shiftwidth = 2
vim.bo.softtabstop = 2
vim.bo.textwidth = 79

vim.wo.cursorcolumn = true
vim.wo.cursorline = true

vim.wo.number = true
vim.wo.relativenumber = true
vim.wo.signcolumn = "yes"
vim.wo.wrap = false

vim.o.backup = false
vim.o.writebackup = false

vim.o.clipboard = "unnamedplus"
vim.o.guicursor = table.concat({
  "n-v-c-sm-o:block",
  "i-ci-ve:ver25",
  "r-cr:hor20",
  "a:blinkwait0-blinkon600-blinkoff400-Cursor/lCursor",
}, ",")
vim.o.termguicolors = true
vim.o.updatetime = 350

vim.opt.fillchars = { eob = " " }
vim.opt.formatoptions:remove "t"
vim.opt.listchars:append { eol = "↲", lead = "·" }
vim.opt.list = true
vim.opt.iskeyword:remove "_"
vim.opt_global.shortmess:append "Iac"
