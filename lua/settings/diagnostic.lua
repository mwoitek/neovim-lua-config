vim.diagnostic.config {
  float = {
    focusable = false,
    scope = "cursor",
    source = true,
  },
  severity_sort = true,
  signs = true,
  underline = false,
  update_in_insert = false,
  virtual_text = {
    severity = { min = vim.diagnostic.severity.WARN },
    source = "if_many",
  },
}

local signs = { Error = " ", Hint = "󰌵 ", Info = " ", Warn = " " }
for type_, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type_
  vim.fn.sign_define(hl, {
    text = icon,
    texthl = hl,
    numhl = hl,
  })
end
