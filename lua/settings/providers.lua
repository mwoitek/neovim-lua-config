local disabled_providers = { "perl", "ruby" }

local python3_host_prog = os.getenv "HOME" .. "/miniconda3/envs/env1/bin/python"
local permissions = vim.fn.getfperm(python3_host_prog)

if permissions:len() > 0 and permissions:sub(3, 3) == "x" then
  vim.g.python3_host_prog = python3_host_prog
else
  table.insert(disabled_providers, "python3")
end

for _, provider in ipairs(disabled_providers) do
  vim.g[string.format("loaded_%s_provider", provider)] = 0
end
