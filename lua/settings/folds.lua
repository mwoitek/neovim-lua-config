-- vim: foldmethod=marker:

-- Basic settings {{{
vim.wo.foldnestmax = 3
vim.wo.foldcolumn = tostring(vim.wo.foldnestmax)

vim.opt.fillchars:append {
  fold = " ",
  foldopen = "󰄼",
  foldclose = "󰄾",
}
vim.opt_global.foldopen:remove "hor"
-- }}}

-- Custom foldtext {{{
---@param s string
---@return string
local escape_special_characters = function(s) return (s:gsub("([%%%-%*%[])", "%%%1")) end

---@diagnostic disable-next-line
function _G.foldtext()
  local line = vim.fn.getline(vim.v.foldstart)
  local commentstring = vim.bo.commentstring
  if commentstring:len() > 0 then
    local pattern = [[^(%s*)]]
      .. escape_special_characters(commentstring:format "X")
        :gsub("X", "(.*)", 1)
        :gsub("%s+", "%%s*")
    local matches = { line:match(pattern) }
    if #matches > 0 then line = table.concat(matches) end
  end
  local marker_start = vim.split(vim.wo.foldmarker, ",")[1]
  local match = line:match([[^(.*)%s*]] .. marker_start)
  if match then line = match end
  local line_len = line:len()
  local win_info = vim.fn.getwininfo(vim.fn.win_getid())[1]
  local win_width = win_info.width - win_info.textoff
  local line_count = vim.v.foldend - vim.v.foldstart + 1
  local line_count_text = string.format(" [%d lines]", line_count)
  local diff = win_width - (line_len + line_count_text:len())
  if diff < 0 then
    line = line:sub(1, line_len + diff - 3) .. "..."
  elseif diff > 0 then
    line = line .. string.rep(" ", diff)
  end
  return line .. line_count_text
end

vim.wo.foldtext = "v:lua.foldtext()"
-- }}}
