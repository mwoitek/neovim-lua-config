vim.api.nvim_create_autocmd("TextYankPost", {
  group = vim.api.nvim_create_augroup("HighlightOnYank", {}),
  callback = function() vim.highlight.on_yank { timeout = 300, on_visual = false } end,
})
