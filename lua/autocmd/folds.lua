-- vim: foldmethod=marker:

local group = vim.api.nvim_create_augroup("DeleteFolds", {})

-- Delete folds {{{
---@param win integer?
local delete_folds = function(win)
  win = win or vim.api.nvim_get_current_win()
  vim.api.nvim_win_call(win, function()
    vim.cmd "setlocal foldmethod=manual"
    vim.cmd "normal! zE"
  end)
end
-- }}}

-- Disable folding for certain filetypes {{{
vim.api.nvim_create_autocmd("FileType", {
  group = group,
  pattern = { "fugitive", "gitcommit" },
  callback = function() delete_folds() end,
})
-- }}}

-- Disable folding for floating windows {{{
-- NOTE: This avoids problems with Telescope windows
vim.api.nvim_create_autocmd("WinEnter", {
  group = group,
  callback = function()
    vim
      .iter(vim.api.nvim_list_wins())
      :filter(function(w) return vim.api.nvim_win_get_config(w).relative ~= "" end)
      :each(delete_folds)
  end,
})
-- }}}
