vim.api.nvim_create_autocmd("BufWritePre", {
  group = vim.api.nvim_create_augroup("DeleteTrailing", {}),
  pattern = {
    "*.toml",
    "*.txt",
    "*.zsh",
    ".gitignore",
  },
  callback = function()
    require("utils.trailing").delete_trailing_whitespace()
    require("utils.trailing").delete_trailing_lines()
  end,
})
