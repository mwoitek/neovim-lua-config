local ft_id = vim.api.nvim_create_augroup("MyFiletypeAutocmds", {})

vim.api.nvim_create_autocmd("FileType", {
  group = ft_id,
  pattern = { "cpp", "rust" },
  command = [[setlocal commentstring=//\ %s]],
})

vim.api.nvim_create_autocmd("FileType", {
  group = ft_id,
  pattern = "gitcommit",
  callback = function() vim.b.editorconfig = false end,
})

vim.api.nvim_create_autocmd("FileType", {
  group = ft_id,
  pattern = "css",
  callback = function()
    ---@diagnostic disable: undefined-field
    local timer = vim.uv.new_timer()
    timer:start(
      1000,
      0,
      vim.schedule_wrap(function()
        timer:stop()
        timer:close()
        vim.opt.iskeyword:remove "-"
      end)
    )
  end,
})
