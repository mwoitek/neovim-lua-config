local autocmd_files = {
  "filetype",
  "folds",
  "session",
  "trailing",
  "yank",
}
local add_prefix_and_load = require("utils.load_files").add_prefix_and_load
add_prefix_and_load(autocmd_files, "autocmd.")
