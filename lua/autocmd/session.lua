local remember_session = vim.api.nvim_create_augroup("RememberSession", {})
vim.api.nvim_create_autocmd("BufWinLeave", {
  group = remember_session,
  pattern = "*.*",
  command = "mkview",
})
vim.api.nvim_create_autocmd("BufWinEnter", {
  group = remember_session,
  pattern = "*.*",
  command = "silent! loadview",
})
