local M = {}

---@param err string
M.print_err = function(err) vim.notify(err, vim.log.levels.ERROR) end

return M
