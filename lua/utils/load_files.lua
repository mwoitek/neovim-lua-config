local M = {}

M.load_files = function(files_tbl)
  if type(files_tbl) ~= "table" then
    vim.notify("Argument of load_files has the wrong type", vim.log.levels.ERROR)
    return
  end

  for _, file in ipairs(files_tbl) do
    local ok, _ = pcall(require, file)
    if not ok then vim.notify("Error loading " .. file, vim.log.levels.ERROR) end
  end
end

M.add_prefix_and_load = function(files_tbl, prefix)
  if type(files_tbl) ~= "table" or type(prefix) ~= "string" then
    vim.notify("Arguments of add_prefix_and_load have the wrong type", vim.log.levels.ERROR)
    return
  end

  local files_with_prefix = vim.tbl_map(function(file) return prefix .. file end, files_tbl)
  M.load_files(files_with_prefix)
end

return M
