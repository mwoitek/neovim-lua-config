local M = {}

---@diagnostic disable-next-line
---@alias fg_tbl { fg: string }

---@param hl_name string
---@param as_table boolean?
---@return string | fg_tbl
function M.get_fg_color(hl_name, as_table)
  local hl_id = vim.api.nvim_get_hl_id_by_name(hl_name)
  local syn_id = vim.fn.synIDtrans(hl_id)
  local fg = vim.fn.synIDattr(syn_id, "fg#")
  return as_table and { fg = fg } or fg
end

---@return { error: fg_tbl, hint: fg_tbl, info: fg_tbl, warn: fg_tbl }
function M.get_diagnostic_colors()
  local colors = {}
  for _, diagnostic in ipairs { "error", "hint", "info", "warn" } do
    local hl_name = "DiagnosticSign" .. (diagnostic:gsub("^%l", string.upper))
    colors[diagnostic] = M.get_fg_color(hl_name, true)
  end
  return colors
end

---@param color string
M.set_fold_colors = function(color)
  local hex_color = M.get_fg_color(color)
  vim.cmd(string.format("hi FoldColumn guifg=%s", hex_color))
  vim.cmd(string.format("hi Folded guifg=%s", hex_color))
end

return M
