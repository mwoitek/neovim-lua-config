local M = {}

local esc = vim.api.nvim_replace_termcodes("<esc>", true, false, true)

function M.get_range()
  vim.api.nvim_feedkeys(esc, "ntx", false)
  local _, start_row, start_col, _ = unpack(vim.fn.getcharpos "'<")
  local _, end_row, end_col, _ = unpack(vim.fn.getcharpos "'>")
  return { start_row - 1, start_col - 1, end_row - 1, end_col }
end

function M.get_text(buf, range)
  local start_row, start_col, end_row, end_col = unpack(range)
  return vim.api.nvim_buf_get_text(buf, start_row, start_col, end_row, end_col, {})
end

local function set_text(buf, range, text)
  local start_row, start_col, end_row, end_col = unpack(range)
  vim.api.nvim_buf_set_text(buf, start_row, start_col, end_row, end_col, text)
end

---@param transform_func function
function M.transform_text(transform_func)
  local range = M.get_range()
  local text = M.get_text(0, range)
  set_text(0, range, transform_func(text))
end

return M
