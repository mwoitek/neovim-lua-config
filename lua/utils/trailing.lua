local function run_and_restore_cursor(cmd)
  local view = vim.fn.winsaveview()
  cmd = string.format("silent keepj keeppatterns %s", cmd)
  vim.cmd(cmd)
  vim.fn.winrestview(view)
end

local M = {}
function M.delete_trailing_whitespace() run_and_restore_cursor [[%s/\s\+$//e]] end
function M.delete_trailing_lines() run_and_restore_cursor [[%s/\n\+\%$//e]] end
return M
