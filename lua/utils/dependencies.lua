local M = {}

M.run_with_external_dependency = function(dependency, callback)
  if vim.fn.executable(dependency) == 0 then
    vim.notify(dependency .. " was not found", vim.log.levels.INFO)
    return
  end
  callback()
end

return M
