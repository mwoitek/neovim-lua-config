require("telescope").setup {
  defaults = {
    borderchars = { "━", "┃", "━", "┃", "┏", "┓", "┛", "┗" },
    layout_config = {
      horizontal = {
        height = 0.97,
        prompt_position = "top",
        width = 0.97,
      },
      vertical = {
        height = 0.97,
        preview_cutoff = 1,
        width = 0.97,
      },
    },
    layout_strategy = "vertical",
    sorting_strategy = "ascending",
  },
  pickers = {
    find_files = { prompt_title = "Open files" },
    oldfiles = { prompt_title = "Open recent files" },
  },
}

local builtin = require "telescope.builtin"

vim.keymap.set("n", "<leader>tc", builtin.commands, {})
vim.keymap.set("n", "<leader>tg", builtin.live_grep, {})
vim.keymap.set("n", "<leader>th", builtin.help_tags, {})
vim.keymap.set("n", "<leader>tl", function() builtin.symbols { sources = { "julia" } } end, {})
vim.keymap.set("n", "<leader>tm", builtin.man_pages, {})
vim.keymap.set("n", "<leader>to", builtin.loclist, {})

local ivy_opts = require("telescope.themes").get_ivy {
  layout_config = { height = 0.3 },
  previewer = false,
  results_title = false,
}
vim.keymap.set("n", "<space>bb", function() builtin.buffers(ivy_opts) end, {})
vim.keymap.set("n", "<space>ff", function() builtin.find_files(ivy_opts) end, {})
vim.keymap.set("n", "<space>fr", function() builtin.oldfiles(ivy_opts) end, {})

require("telescope").load_extension "notify"
vim.keymap.set("n", "<leader>tn", "<cmd>Telescope notify<cr>", {})
