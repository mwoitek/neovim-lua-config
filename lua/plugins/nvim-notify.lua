local notify = require "notify"

notify.setup {
  icons = {
    DEBUG = "",
    ERROR = "",
    INFO = "",
    TRACE = "✎",
    WARN = "",
  },
  level = vim.log.levels.INFO,
  minimum_width = 35,
  max_width = 50,
  max_height = 25,
  render = "wrapped-compact",
  stages = "static",
  timeout = 4000,
  on_open = function(win)
    local config = vim.api.nvim_win_get_config(win)
    config.border = { "┏", "━", "┓", "┃", "┛", "━", "┗", "┃" }
    vim.api.nvim_win_set_config(win, config)
  end,
}

vim.notify = vim.schedule_wrap(notify.notify)
