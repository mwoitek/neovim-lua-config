local file_path = os.getenv "HOME" .. "/.config/nvim/theme_variant"

local function read_variant()
  local file = io.open(file_path, "r")
  if not file then return "dark" end
  local variant = file:read "*all"
  file:close()
  return variant
end

vim.o.background = read_variant()

vim.g.everforest_background = "hard"
vim.g.everforest_better_performance = 1
vim.g.everforest_disable_italic_comment = 1
vim.g.everforest_ui_contrast = "high"

vim.cmd "colorscheme everforest"
require("utils.colors").set_fold_colors "Yellow"

local function write_variant(variant)
  local file = io.open(file_path, "w")
  if not file then return end
  file:write(variant)
  file:close()
end

local function toggle_theme()
  vim.o.background = (vim.o.background == "dark") and "light" or "dark"
  vim.cmd "colorscheme everforest"
  write_variant(vim.o.background)
end

vim.keymap.set("n", "<space>tl", toggle_theme, {})
