require("ibl").setup {
  indent = {
    highlight = {
      "Red",
      "Yellow",
      "Blue",
      "Orange",
      "Green",
      "Purple",
    },
    char = "│",
  },
  scope = { enabled = os.getenv "TMUX" == nil },
  exclude = {
    filetypes = {
      "checkhealth",
      "fugitive",
      "gitcommit",
      "help",
      "man",
      "TelescopePrompt",
      "TelescopeResults",
      "text",
      "",
    },
  },
}
