require("nvim-treesitter.configs").setup {
  ensure_installed = {
    "bash",
    "c",
    "cpp",
    "css",
    "dockerfile",
    "fortran",
    "gdscript",
    "go",
    "gomod",
    "gosum",
    "haskell",
    "html",
    "javascript",
    "julia",
    "kdl",
    "latex",
    "lua",
    "markdown",
    "markdown_inline",
    "python",
    "query",
    "rust",
    "sql",
    "typescript",
    "vim",
    "yaml",
    -- "r",
  },
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  incremental_selection = { enable = false },
  textobjects = {
    move = {
      enable = true,
      set_jumps = true,
      goto_next_start = {
        ["<leader>b"] = "@block.outer",
        ["]]"] = "@class.outer",
        ["]m"] = "@function.outer",
      },
      goto_previous_start = {
        ["<leader>B"] = "@block.outer",
        ["[["] = "@class.outer",
        ["[m"] = "@function.outer",
      },
      goto_next_end = {
        ["]["] = "@class.outer",
        ["]M"] = "@function.outer",
      },
      goto_previous_end = {
        ["[]"] = "@class.outer",
        ["[M"] = "@function.outer",
      },
    },
    select = {
      enable = true,
      lookahead = true,
      keymaps = {
        ["aa"] = "@parameter.outer",
        ["ia"] = "@parameter.inner",
        ["ac"] = "@class.outer",
        ["ic"] = "@class.inner",
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["ak"] = "@block.outer",
        ["ik"] = "@block.inner",
        ["io"] = "@id.inner",
      },
      selection_modes = {
        ["@block.inner"] = "V",
        ["@block.outer"] = "V",
        ["@class.inner"] = "V",
        ["@class.outer"] = "V",
        ["@function.inner"] = "V",
        ["@function.outer"] = "V",
      },
    },
    swap = { enable = false },
  },
}

-- Some keybindings have to be defined manually.
-- https://github.com/nvim-treesitter/nvim-treesitter-textobjects/issues/131
local create_keybinding = function(mode, kb, query)
  local cmd = '<cmd>lua require("nvim-treesitter.textobjects.select").'
  cmd = cmd .. 'select_textobject("' .. query .. '", nil, "' .. mode .. '")<cr>'
  vim.keymap.set(mode, kb, cmd, { buffer = 0 })
end

local create_keybindings = function(kb, query)
  create_keybinding("o", kb, query)
  create_keybinding("x", kb, query)
end

vim.api.nvim_create_autocmd("FileType", {
  group = vim.api.nvim_create_augroup("FiletypeKeybindings", { clear = false }),
  pattern = "rmd",
  callback = function()
    create_keybindings("aa", "@parameter.outer")
    create_keybindings("ia", "@parameter.inner")
    create_keybindings("af", "@function.outer")
    create_keybindings("if", "@function.inner")
  end,
})

vim.cmd "hi link @cell.delimiter TSTitle"
