vim.g.sonokai_better_performance = 1
vim.g.sonokai_disable_italic_comment = 1
vim.g.sonokai_float_style = "dim"
vim.g.sonokai_style = "espresso"
vim.g.sonokai_transparent_background = 0
vim.cmd "colorscheme sonokai"
require("utils.colors").set_fold_colors "Yellow"
