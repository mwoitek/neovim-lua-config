require("lualine").setup {
  options = {
    icons_enabled = true,
    theme = "auto",
    globalstatus = true,
    component_separators = "",
    section_separators = "",
    always_divide_middle = true,
  },
  sections = {
    lualine_a = {
      { "mode", fmt = function(mode) return mode:sub(1, 1) end },
    },
    lualine_b = {
      "branch",
      {
        "diff",
        symbols = { added = " ", modified = "󰝤 ", removed = " " },
      },
    },
    lualine_c = {
      "filetype",
      "filename",
      {
        "b:spell_lang",
        color = function() return { fg = vim.b.spell_fg, bg = vim.b.spell_bg, gui = "bold" } end,
      },
    },
    lualine_x = {
      {
        "diagnostics",
        sources = { "nvim_diagnostic" },
        sections = { "error", "hint", "info", "warn" },
        diagnostics_color = require("utils.colors").get_diagnostic_colors(),
        symbols = { error = " ", hint = "󰌵 ", info = " ", warn = " " },
      },
      {
        "b:action_count",
        icon = "󰠠",
        color = require("utils.colors").get_fg_color("Orange", true),
      },
    },
    lualine_y = { "location", "encoding", "fileformat" },
    lualine_z = {},
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = { "filename" },
    lualine_x = {},
    lualine_y = {},
    lualine_z = {},
  },
  extensions = {},
  tabline = {},
}
