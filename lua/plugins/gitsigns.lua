local gs = require "gitsigns"
gs.setup {
  attach_to_untracked = false,
  signcolumn = true,
  on_attach = function(bufnr)
    vim.keymap.set("n", "<space>gl", gs.toggle_linehl, { buffer = bufnr })
    vim.keymap.set("n", "<space>hp", gs.preview_hunk, { buffer = bufnr })
    vim.keymap.set("n", "<space>td", gs.toggle_deleted, { buffer = bufnr })
    local opts = { buffer = bufnr, expr = true, noremap = true, silent = true }
    vim.keymap.set("n", "[c", "&diff ? '[c' : ':Gitsigns prev_hunk<cr>'", opts)
    vim.keymap.set("n", "]c", "&diff ? ']c' : ':Gitsigns next_hunk<cr>'", opts)
  end,
}
