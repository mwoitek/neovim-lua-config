vim.g.rainbow_delimiters = {
  highlight = {
    "Red",
    "Yellow",
    "Blue",
    "Orange",
    "Green",
    "Purple",
    "RainbowDelimiterCyan",
  },
}
