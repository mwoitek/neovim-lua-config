require("dressing").setup {
  input = {
    insert_only = false,
    start_in_insert = false,
    title_pos = "center",
    border = "single",
    relative = "editor",
    prefer_width = 0.65,
    mappings = {
      n = {
        ["<Esc>"] = "Close",
        ["<CR>"] = "Confirm",
        k = "HistoryPrev",
        j = "HistoryNext",
      },
      i = {
        ["<C-c>"] = "Close",
        ["<CR>"] = "Confirm",
        ["<C-p>"] = "HistoryPrev",
        ["<C-n>"] = "HistoryNext",
      },
    },
  },
  select = {
    backend = { "telescope", "builtin" },
    telescope = require("telescope.themes").get_ivy {
      layout_config = { height = 0.25 },
      results_title = false,
    },
  },
}
