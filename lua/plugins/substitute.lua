local substitute = require "substitute"
local exchange = require "substitute.exchange"

substitute.setup {
  highlight_substituted_text = { enabled = false },
}

vim.keymap.set("n", "gr", substitute.operator, {})
vim.keymap.set("n", "grr", substitute.line, {})

vim.keymap.set("n", "gx", exchange.operator, {})
vim.keymap.set("n", "gxx", exchange.line, {})
vim.keymap.set("x", "gX", exchange.visual, {})
