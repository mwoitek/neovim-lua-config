vim.g.barbar_auto_setup = false
local custom_separator = { left = "│", right = "" }

require("barbar").setup {
  animation = false,
  auto_hide = false,
  clickable = false,
  focus_on_close = "previous",
  icons = {
    button = false,
    alternate = { separator = custom_separator },
    current = { separator = custom_separator },
    inactive = { separator = custom_separator },
    visible = { separator = custom_separator },
  },
  sidebar_filetypes = { NvimTree = true },
  tabpages = false,
}

vim.keymap.set("n", "<space>bd", "<cmd>BufferClose<cr>", { silent = true })
vim.keymap.set("n", "<space>bn", "<cmd>BufferNext<cr>", { silent = true })
vim.keymap.set("n", "<space>bp", "<cmd>BufferPrevious<cr>", { silent = true })

vim.keymap.set("n", "<space>bk", "", {})
vim.keymap.set("n", "<space>bk", "<cmd>BufferClose!<cr>", { silent = true })

local hl_groups = {
  "BufferAlternateSign",
  "BufferAlternateSignRight",
  "BufferCurrent",
  "BufferCurrentSign",
  "BufferCurrentSignRight",
  "BufferInactiveSign",
  "BufferInactiveSignRight",
  "BufferVisibleSign",
  "BufferVisibleSignRight",
}
local inactive_bg = vim.api.nvim_get_hl(0, { name = "BufferInactive", create = false }).bg
local inactive_bg_rgb = string.format("#%x", inactive_bg)
for _, hl_group in ipairs(hl_groups) do
  vim.cmd.highlight(string.format("%s guibg=%s", hl_group, inactive_bg_rgb))
end
