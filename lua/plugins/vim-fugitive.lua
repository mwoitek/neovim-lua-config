vim.keymap.set("n", "<space>gd", "<cmd>Gdiffsplit<cr>", { silent = true })
vim.keymap.set("n", "<space>gp", "<cmd>Git push<cr>", { silent = true })
