vim.g.gruvbox_material_background = "medium"
vim.g.gruvbox_material_better_performance = 1
vim.g.gruvbox_material_disable_italic_comment = 1
vim.g.gruvbox_material_enable_italic = 0
vim.g.gruvbox_material_float_style = "dim"
vim.g.gruvbox_material_foreground = "mix"
vim.g.gruvbox_material_menu_selection_background = "green"
vim.g.gruvbox_material_transparent_background = 0
vim.g.gruvbox_material_ui_contrast = "high"
vim.cmd "colorscheme gruvbox-material"
require("utils.colors").set_fold_colors "Yellow"
