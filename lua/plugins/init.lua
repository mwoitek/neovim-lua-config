local ft_treesitter = {
  "c",
  "cpp",
  "css",
  "dockerfile",
  "fortran",
  "gdscript",
  "go",
  "gomod",
  "gosum",
  "haskell",
  "html",
  "javascript",
  "julia",
  "kdl",
  "lua",
  "markdown",
  "python",
  "query",
  "rust",
  "sh",
  "sql",
  "tex",
  "typescript",
  "vim",
  "yaml",
  -- "r",
  -- "rmd",
}

require("lazy").setup {
  {
    "sainnhe/everforest",
    priority = 1000,
    config = function() require "plugins.everforest" end,
    enabled = false,
  },
  {
    "sainnhe/gruvbox-material",
    priority = 1000,
    config = function() require "plugins.gruvbox-material" end,
    enabled = false,
  },
  {
    "sainnhe/sonokai",
    priority = 1000,
    config = function() require "plugins.sonokai" end,
  },
  {
    "nvim-lualine/lualine.nvim",
    dependencies = "kyazdani42/nvim-web-devicons",
    config = function() require "plugins.lualine" end,
  },
  {
    "romgrk/barbar.nvim",
    dependencies = "kyazdani42/nvim-web-devicons",
    config = function() require "plugins.barbar" end,
  },
  {
    "rcarriga/nvim-notify",
    config = function() require "plugins.nvim-notify" end,
    event = "VeryLazy",
  },

  {
    "kyazdani42/nvim-tree.lua",
    dependencies = "kyazdani42/nvim-web-devicons",
    config = function() require "plugins.nvim-tree" end,
    keys = "<space>nt",
  },

  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    dependencies = "nvim-treesitter/nvim-treesitter-textobjects",
    config = function() require "plugins.nvim-treesitter" end,
    ft = ft_treesitter,
  },
  {
    "Wansmer/treesj",
    dependencies = "nvim-treesitter/nvim-treesitter",
    config = function() require "plugins.treesj" end,
    ft = ft_treesitter,
  },
  {
    "hiphish/rainbow-delimiters.nvim",
    dependencies = "nvim-treesitter/nvim-treesitter",
    config = function() require "plugins.rainbow-delimiters" end,
    ft = ft_treesitter,
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    dependencies = "nvim-treesitter/nvim-treesitter",
    config = function() require "plugins.indent-blankline" end,
    ft = ft_treesitter,
  },

  { "tpope/vim-repeat", event = "VeryLazy" },
  { "tpope/vim-unimpaired", keys = { "[", "]", "=" } },

  {
    "tpope/vim-surround",
    config = function() require "plugins.vim-surround" end,
    keys = { "cs", "ds", "ys", { "S", mode = "v" } },
  },
  {
    "windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup { disable_in_macro = false } end,
    keys = {
      { "(", mode = "i" },
      { "[", mode = "i" },
      { "{", mode = "i" },
      { [["]], mode = "i" },
      { [[']], mode = "i" },
    },
  },

  {
    "tpope/vim-fugitive",
    config = function() require "plugins.vim-fugitive" end,
    cmd = "Gedit",
  },
  {
    "lewis6991/gitsigns.nvim",
    dependencies = "nvim-lua/plenary.nvim",
    config = function() require "plugins.gitsigns" end,
  },

  {
    "gbprod/substitute.nvim",
    config = function() require "plugins.substitute" end,
    keys = { "gr", "gx", { "gX", mode = "x" } },
  },

  {
    "chaoren/vim-wordmotion",
    init = function() vim.g.wordmotion_prefix = "<leader>" end,
    ft = {
      "go",
      "javascript",
      "python",
      "typescript",
    },
  },
  {
    "justinmk/vim-sneak",
    init = function() require "plugins.vim-sneak" end,
    event = "VeryLazy",
  },
  {
    "smoka7/hop.nvim",
    config = function() require "plugins.hop" end,
    keys = { "<leader><leader>", "gs" },
  },

  {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.8",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-symbols.nvim",
      {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        config = function() require("telescope").load_extension "fzf" end,
      },
    },
    config = function() require "plugins.telescope" end,
    keys = {
      "<leader>t",
      "<space>ca",
      "<space>ff",
      "<space>fr",
    },
  },

  {
    "folke/todo-comments.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope.nvim",
    },
    config = function() require "plugins.todo-comments" end,
    ft = ft_treesitter,
  },

  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      "L3MON4D3/LuaSnip",
      "onsails/lspkind-nvim",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "kdheepak/cmp-latex-symbols",
      "saadparwaiz1/cmp_luasnip",
    },
    config = function() require "plugins.completion" end,
    event = "InsertEnter",
  },

  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      {
        "j-hui/fidget.nvim",
        tag = "v1.4.5",
        opts = {},
      },
      {
        "stevearc/dressing.nvim",
        dependencies = "nvim-telescope/telescope.nvim",
        config = function() require "plugins.dressing" end,
      },
      "b0o/schemastore.nvim",
    },
    config = function() require "lsp.config" end,
    ft = {
      "c",
      "cpp",
      "css",
      "dockerfile",
      "fortran",
      "gdscript",
      "go",
      "haskell",
      "html",
      "javascript",
      "json",
      "julia",
      "lua",
      "python",
      "r",
      "rmd",
      "rust",
      "sh",
      "sql",
      "tex",
      "typescript",
    },
  },

  {
    "jpalardy/vim-slime",
    config = function() require "plugins.vim-slime" end,
    ft = { "julia", "python", "r" },
  },

  {
    "lervag/vimtex",
    init = function() require "plugins.vimtex" end,
    ft = "tex",
  },

  {
    "habamax/vim-godot",
    init = function() require "plugins.vim-godot" end,
    ft = { "gdscript", "gsl" },
    enabled = false,
  },

  {
    "alvan/vim-closetag",
    config = function() require "plugins.vim-closetag" end,
    ft = {
      "html",
      "javascript",
      "markdown",
      "rmd",
      "typescript",
    },
  },

  {
    "mwoitek/scratcher.nvim",
    branch = "dev", -- master
    config = function() require "plugins.scratcher" end,
    keys = {
      "<localleader>d",
      "<localleader>s",
      "<space>s",
      { "<space>sp", mode = "x" },
    },
  },
}
