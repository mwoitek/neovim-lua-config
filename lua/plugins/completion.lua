local cmp = require "cmp"
local luasnip = require "luasnip"

local completion_prev = function(fallback)
  if cmp.visible() then
    cmp.select_prev_item()
  elseif luasnip.jumpable(-1) then
    luasnip.jump(-1)
  else
    fallback()
  end
end

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0
    and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match "%s" == nil
end

local completion_next = function(fallback)
  if cmp.visible() then
    cmp.select_next_item()
  elseif luasnip.expand_or_jumpable() then
    luasnip.expand_or_jump()
  elseif has_words_before() then
    cmp.complete()
  else
    fallback()
  end
end

local keyword_length = 3
local sources_names = {
  "nvim_lsp",
  "buffer",
  "path",
  "luasnip",
}

local sources = {}
for _, name in ipairs(sources_names) do
  table.insert(sources, {
    name = name,
    keyword_length = keyword_length,
  })
end

local lspkind_options = {
  mode = "symbol_text",
  maxwidth = 50,
  menu = {
    nvim_lsp = "[LSP]",
    buffer = "[Buff]",
    path = "[Path]",
    luasnip = "[Snip]",
    latex_symbols = "[TeX]",
  },
}

cmp.setup {
  completion = { autocomplete = false },
  snippet = { expand = function(args) luasnip.lsp_expand(args.body) end },
  mapping = {
    ["<CR>"] = cmp.mapping.confirm { behavior = cmp.ConfirmBehavior.Replace },
    ["<C-p>"] = cmp.mapping(completion_prev, { "i", "s" }),
    ["<S-Tab>"] = cmp.mapping(completion_prev, { "i", "s" }),
    ["<C-n>"] = cmp.mapping(completion_next, { "i", "s" }),
    ["<Tab>"] = cmp.mapping(completion_next, { "i", "s" }),
  },
  sources = sources,
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      local kind = require("lspkind").cmp_format(lspkind_options)(entry, vim_item)
      local strings = vim.split(kind.kind, "%s", { trimempty = true })
      kind.kind = " " .. (strings[1] or "") .. " "
      kind.menu = "    (" .. (strings[2] or "") .. ")"
      return kind
    end,
  },
  performance = { max_view_entries = 6 },
  preselect = cmp.PreselectMode.None,
  window = {
    completion = {
      col_offset = -3,
      side_padding = 0,
      winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None",
    },
    documentation = cmp.config.disable,
  },
}

table.insert(sources, {
  name = "latex_symbols",
  keyword_length = keyword_length,
})
cmp.setup.filetype("julia", { sources = sources })

-- Trick for avoiding lag. Adapted from
-- https://github.com/hrsh7th/nvim-cmp/issues/598

---@diagnostic disable: undefined-field
local timer = vim.uv.new_timer()
local DEBOUNCE_DELAY = vim.o.updatetime

local debounce = function()
  timer:stop()
  timer:start(
    DEBOUNCE_DELAY,
    0,
    vim.schedule_wrap(function()
      if not cmp.visible() then -- Fixes weird behavior in CSS files
        cmp.complete { reason = cmp.ContextReason.Auto }
      end
    end)
  )
end

vim.api.nvim_create_autocmd("CursorHoldI", {
  group = vim.api.nvim_create_augroup("CmpDebounce", {}),
  callback = debounce,
})
