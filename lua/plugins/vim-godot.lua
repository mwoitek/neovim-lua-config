vim.api.nvim_create_autocmd("FileType", {
  group = vim.api.nvim_create_augroup("FiletypeKeybindings", { clear = false }),
  pattern = "gdscript",
  callback = function()
    vim.keymap.set("n", "<leader>gc", "<cmd>GodotRunCurrent<cr>", { buffer = 0 })
    vim.keymap.set("n", "<leader>gf", "<cmd>GodotRunFZF<cr>", { buffer = 0 })
    vim.keymap.set("n", "<leader>gl", "<cmd>GodotRunLast<cr>", { buffer = 0 })
    vim.keymap.set("n", "<leader>gr", "<cmd>GodotRun<cr>", { buffer = 0 })
  end,
})
