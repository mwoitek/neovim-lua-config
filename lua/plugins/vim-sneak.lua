vim.g["sneak#use_ic_scs"] = 1

vim.keymap.set("", "F", "<plug>Sneak_F", {})
vim.keymap.set("", "T", "<plug>Sneak_T", {})
vim.keymap.set("", "f", "<plug>Sneak_f", {})
vim.keymap.set("", "t", "<plug>Sneak_t", {})

-- use square brackets to find any bracket

local function get_first_position(positions, back)
  local num_positions = #positions
  if num_positions == 0 then return nil end
  if num_positions == 1 then return positions[1] end

  local cmp_func
  if back then
    cmp_func = function(x, y) return x[1] > y[1] or (x[1] == y[1] and x[2] > y[2]) end
  else
    cmp_func = function(x, y) return x[1] < y[1] or (x[1] == y[1] and x[2] < y[2]) end
  end

  local first = positions[1]
  for i = 2, num_positions do
    if cmp_func(positions[i], first) then first = positions[i] end
  end
  return first
end

local function search_delim(delims, back, till)
  local flags = "nW" .. (back and "b" or "")
  local search_results = vim.tbl_map(function(d) return vim.fn.searchpos(d, flags) end, delims)

  local positions = vim.tbl_filter(function(p) return p[1] ~= 0 or p[2] ~= 0 end, search_results)
  local pos = get_first_position(positions, back)

  if not pos then return end

  if back or vim.api.nvim_get_mode().mode ~= "no" then pos[2] = pos[2] - 1 end
  if till then pos[2] = pos[2] + (back and 1 or -1) end
  pcall(vim.api.nvim_win_set_cursor, 0, pos)
end

local function search_opening(back, till) search_delim({ "(", "[", "{" }, back, till) end
local function search_closing(back, till) search_delim({ ")", "]", "}" }, back, till) end

local SEARCH_ARGS = {
  F = { true, false },
  T = { true, true },
  f = { false, false },
  t = { false, true },
}
for l, args in pairs(SEARCH_ARGS) do
  vim.keymap.set("", l .. "[", function()
    local count = vim.v.count > 0 and vim.v.count or 1
    for _ = 1, count do
      search_opening(args[1], args[2])
    end
  end, {})
  vim.keymap.set("", l .. "]", function()
    local count = vim.v.count > 0 and vim.v.count or 1
    for _ = 1, count do
      search_closing(args[1], args[2])
    end
  end, {})
end
