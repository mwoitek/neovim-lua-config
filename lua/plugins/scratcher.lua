local scratcher = require "scratcher"

scratcher.setup {
  position = "right",
  width = 0.45,
  -- start_in_insert = true,
  -- auto_hide = { enable = true, timeout = 1.2 },
}

vim.keymap.set("n", "<space>sn", "<cmd>Scratch<cr>", {})
vim.keymap.set("n", "<space>sc", "<cmd>ScratchClear<cr>", {})
vim.keymap.set("n", "<space>sx", "<cmd>ScratchToggle<cr>", {})

vim.keymap.set("n", "<localleader>s", "<cmd>ScratchPaste<cr>", {})
vim.keymap.set("x", "<space>sp", "<cmd>ScratchPaste<cr>", {})

vim.keymap.set("n", "<localleader>d", "<cmd>ScratchMove<cr>", {})
vim.keymap.set("x", "<space>sd", "<cmd>ScratchMove<cr>", {})
