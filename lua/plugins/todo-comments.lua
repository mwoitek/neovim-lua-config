local todo = require "todo-comments"
todo.setup {
  signs = false,
  highlight = {
    multiline = false,
    keyword = "bg",
    after = "",
    pattern = [[.*<(KEYWORDS)\s*]],
  },
}

vim.keymap.set("n", "[t", todo.jump_prev, {})
vim.keymap.set("n", "]t", todo.jump_next, {})
vim.keymap.set("n", "<leader>tt", "<cmd>TodoTelescope<cr>", { silent = true })
