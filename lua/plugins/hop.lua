local hop = require "hop"
hop.setup {}

local after_cursor = require("hop.hint").HintDirection.AFTER_CURSOR
local before_cursor = require("hop.hint").HintDirection.BEFORE_CURSOR

vim.keymap.set(
  "n",
  "<leader><leader>b",
  function() hop.hint_words { direction = before_cursor } end,
  {}
)
vim.keymap.set(
  "n",
  "<leader><leader>w",
  function() hop.hint_words { direction = after_cursor } end,
  {}
)

vim.keymap.set(
  "n",
  "gsk",
  function() hop.hint_lines_skip_whitespace { direction = before_cursor } end,
  {}
)
vim.keymap.set(
  "n",
  "gsj",
  function() hop.hint_lines_skip_whitespace { direction = after_cursor } end,
  {}
)
