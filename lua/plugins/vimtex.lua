vim.g.vimtex_complete_enabled = 0
vim.g.vimtex_matchparen_enabled = 1
vim.g.vimtex_syntax_enabled = 0
vim.g.vimtex_view_forward_search_on_start = 0
vim.g.vimtex_view_method = "zathura"
