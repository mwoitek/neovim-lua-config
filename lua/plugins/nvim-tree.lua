require("nvim-tree").setup {
  view = {
    width = 30,
    preserve_window_proportions = true,
  },
}

vim.keymap.set("n", "<space>nt", "<cmd>NvimTreeToggle<cr>", { silent = true })

vim.api.nvim_create_autocmd("BufEnter", {
  group = vim.api.nvim_create_augroup("NvimTreeAutoClose", {}),
  callback = function()
    if #vim.api.nvim_list_wins() == 1 and vim.api.nvim_buf_get_name(0):find "NvimTree" then
      vim.cmd.quit()
    end
  end,
})
