if vim.fn.executable "tmux" == 0 then return end
vim.g.slime_target = "tmux"

vim.g.slime_paste_file = vim.fn.tempname()
vim.g.slime_bracketed_paste = 1
vim.g.slime_dont_ask_default = 1
vim.g.slime_no_mappings = 1

vim.api.nvim_create_autocmd("FileType", {
  group = vim.api.nvim_create_augroup("SlimeConfig", {}),
  pattern = { "julia", "python", "r" },
  callback = function()
    vim.b.slime_cell_delimiter = vim.bo.commentstring:format "%%"
    require("repl.vim-slime-extra").setup()
    require("repl.vim-slime-tmux").add_keybindings()
  end,
})
