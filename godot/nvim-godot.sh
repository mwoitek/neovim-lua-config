#!/bin/bash
p="$1"
f="$2"

[[ -z "$p" || -z "$f" ]] && exit 1
[[ ! -d "$p" ]] && exit 1
[[ ! -f "$f" ]] && exit 1

if ! pgrep -af -u "$USER" 'nvim --listen' &>/dev/null; then
  template_file="$(realpath "$0" | xargs -I{} dirname {})/tmuxp-template.yaml"
  tmp_file="$(mktemp -p /tmp tmuxp-workspace-file-XXXXX.yml)"
  sed "s,PROJECTDIR,${p}," "$template_file" >>"$tmp_file"
  tmuxp load -dy "$tmp_file"
  sleep 3s
fi

nvim --server "${XDG_CACHE_HOME}/nvim/godot.pipe" --remote-send "<C-\><C-N>:e ${f}<CR>"
