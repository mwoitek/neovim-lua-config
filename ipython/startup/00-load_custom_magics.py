# pyright: basic

"""Load my custom magics."""

import sys
from pathlib import Path

from IPython.core.getipython import get_ipython

curr_file = Path(__file__).resolve()
sys.path.append(str(curr_file.parents[1]))

ipython = get_ipython()
assert ipython is not None
ipython.run_line_magic("load_ext", "custom_magics")
ipython.run_line_magic("matplotlib_maybe", "")
