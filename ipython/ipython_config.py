# pyright: basic
# Configuration file for IPython
c = get_config()  # pyright: ignore
c.PlainTextFormatter.pprint = True
c.TerminalIPythonApp.code_to_run = "clear"
c.TerminalIPythonApp.display_banner = False
c.TerminalIPythonApp.force_interact = True
c.TerminalInteractiveShell.confirm_exit = False
c.TerminalInteractiveShell.quiet = True
c.TerminalInteractiveShell.separate_in = ""
c.TerminalInteractiveShell.separate_out = ""
c.TerminalInteractiveShell.separate_out2 = ""
c.TerminalInteractiveShell.term_title = False
