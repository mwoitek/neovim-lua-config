# pyright: basic

"""My custom IPython magics."""

from contextlib import closing, redirect_stdout
from importlib.util import find_spec
from io import StringIO
from pathlib import Path
from typing import cast
from warnings import filterwarnings

from IPython.core.interactiveshell import InteractiveShell
from IPython.core.magic import Magics, line_magic, magics_class


@magics_class
class MyCustomMagics(Magics):
    @line_magic
    def matplotlib_maybe(self, line: str) -> None:
        """Custom version of %matplotlib magic."""
        if find_spec("matplotlib") is None:
            print("matplotlib could not be found")
            return

        from matplotlib.backends.registry import BackendFilter, BackendRegistry

        backend = line.strip().split(" ")[0].lower()
        if not backend:
            backend = "tkagg"

        if backend not in BackendRegistry().list_builtin(filter_=BackendFilter.INTERACTIVE):
            print(f"Not a valid interactive backend: {backend}")
            return

        import matplotlib as mpl
        import matplotlib.pyplot as plt

        mpl.use(backend)
        plt.ion()

    @line_magic
    def quiet_run(self, line: str) -> None:
        """Custom version of %run that suppresses all output."""
        file_input = line.strip().split(" ")[0]
        if not file_input:
            print("No file was specified")
            return

        file_path = Path(file_input)
        if not (file_path.exists() and file_path.is_file()):
            print(f"Not a valid file: {file_path}")
            return

        has_matplotlib = find_spec("matplotlib") is not None
        if has_matplotlib:
            import matplotlib as mpl

            filterwarnings("ignore", message=r"^FigureCanvasAgg", category=UserWarning)
            backend = mpl.get_backend()
            mpl.use("agg")

        shell = cast(InteractiveShell, self.shell)
        with closing(StringIO()) as _s:
            with redirect_stdout(_s):
                shell.run_line_magic("run", f"-i {file_path}")

        if has_matplotlib:
            mpl.use(backend)


def load_ipython_extension(ipython: InteractiveShell) -> None:
    ipython.register_magics(MyCustomMagics)
