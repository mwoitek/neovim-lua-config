;; inherits: julia
;; extends

((line_comment) @cell.delimiter
 (#lua-match? @cell.delimiter "# %%%%")
 (#offset! @cell.delimiter 0 2 0 0))
