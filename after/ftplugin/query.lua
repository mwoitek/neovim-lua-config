vim.bo.commentstring = ";; %s"
vim.bo.lisp = false
vim.opt_local.iskeyword:remove { "-", ":" }
