vim.opt.formatoptions:append "ro"
vim.keymap.set("i", "]a", "->", { buffer = 0 })

require("repl.code_cells").setup()
